with Ada.Text_IO;
with Ada.Integer_Text_IO;
use Ada.Integer_Text_IO;
use Ada.Text_IO;
 
procedure Test_Ackermann is
   output : integer;
   n : integer;
   m : integer;

begin
	Put_Line("Enter a positive number");
	
	Get(m);
	n := (m + 5 * 6 -(3*8))/5;
	Put(n);
	m := 9 * n;
        Put(m);
	
end Test_Ackermann;
