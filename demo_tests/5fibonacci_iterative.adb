with Ada.Text_IO;
with Ada.Integer_Text_IO;
use Ada.Integer_Text_IO;
use Ada.Text_IO;

procedure Test_Fibonacci is
   
   n : integer;
   m : integer;
   
   procedure Fibonacci (input: in integer; output: out integer ) is
      a : integer := 0;
      b : integer := 1;
      Sum  : integer;
   begin
      for I in 1..input loop
         Sum  := a + b;
         b := a;
         a := Sum;
      end loop;
      output := Sum;
   end Fibonacci;

begin
	Put_Line("Enter the position of the fibonacci number you want");
	Get(n);
	Fibonacci(n,m);
	Put(m);
end Test_Fibonacci;
