# ADA COMPILER

## Dependencies
Library Implementation of _printf_ has been used for printing floats. Compiling assembly code requires gcc-multlib
```
sudo apt-get install gcc-multilib
```


## Running

For running code *tests/12newton-raphson.adb* run
```
make code testFile=12newton-raphson
./tests/12newton-raphson
```

For Three Address Code for *tests/1quicksort.adb* run
```
make emitTac testFile=1quicksort
```

For parse-tree file *tests/test1.adb* run
```
make test testFile=test1
```

Similarly for all test files in *tests/*

## Parser Specifications

Tokens and grammer are specified in files *parser/adaTokens.py* and *parser/grammer.py* 

### parser.py

Takes as input the name of the file and passes to lexer(*parser/lex.py*) to extract tokens. The tokens are passed to parser(*parser/yacc.py*) which produces *tests/file.reduce* containing all the reduce rule instances. Finally, this is passed to *make_ParseTree* (*parser/makeDot.py*) which converts *tests/file.reduce* into a parse tree and saves the output graph as *tests/file.png*.


## Acknowledgement
Inspiration and Grammer Source: https://github.com/ayusek/ada-compiler