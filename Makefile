test :
	@cd parser && ./parser.py ../tests/$(testFile).adb
	@cd tests && xdg-open $(testFile).png

ast : 
	@cd semantic && ./parser.py ../tests/$(testFile).adb
	@cd tests && dot -Tpng final.dot -o $(testFile)_ast.png
	@cd tests && xdg-open $(testFile)_ast.png

emitTac : 
	@cd tac && ./parser.py ../tests/$(testFile).adb

code : 
	@cd code && ./parser.py ../tests/$(testFile).adb
	@nasm -felf -g tests/$(testFile).asm 2>&1
	@gcc -m32 -e main tests/$(testFile).o -o tests/$(testFile) 

.PHONY : code emitTac ast test

clean :
	rm -rf code/*.pyc code/*.out code/parsetab.py semantic/*.pyc semantic/*.out semantic/parsetab.py tac/*.pyc tac/*.out tac/parsetab.py parser/*.pyc parser/*.out parser/parsetab.py tests/*.dot tests/*.reduce tests/*.ali tests/*.o tests/*.png tests/symTables/*

