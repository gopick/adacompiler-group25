with Ada.Text_IO;
with Ada.Integer_Text_IO;
use Ada.Integer_Text_IO;
use Ada.Text_IO;

procedure Test_Binary_Search is
   arr : array (1..5) of integer :=(4,5,6,7,11);
   search_element : integer;
   output : integer;

   procedure Search (val : in  integer; index : out integer) is
      low  : integer := 1;
      high : integer := 5;
      mid  : integer;
      
   begin 
      index := -1;
      while(low <= high) loop
            mid := (low + high) / 2;
            if arr(mid) = val then
               index := mid;
	       high := low-1;
            elsif arr(mid) < val then
               low := mid + 1;
            else
               high := mid -1;
            end if;
      end loop;
   end Search;

begin
   Put_Line("Enter the element you want to search in the array");
   Get(search_element);
   Search(search_element,output);
   Put(output);
end Test_Binary_Search;
