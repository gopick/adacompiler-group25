with Ada.Text_IO;
with Ada.Integer_Text_IO;
use Ada.Integer_Text_IO;
use Ada.Text_IO;

procedure fileio is

   fd : integer;

begin

   open("myfile.txt", fd);
   write("THIS IS CHECK", fd);
   close(fd);

end fileio;
