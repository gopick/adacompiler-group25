with Ada.Numerics.Elementary_Functions;
use Ada.Numerics.Elementary_Functions;
with Ada.Float_Text_Io; use Ada.Float_Text_Io;
with Ada.Text_IO; use Ada.Text_IO;
 
procedure Trig is
   Degrees_Cycle : Float := 360.0;
   Radians_Cycle : Float := 2.0 * Ada.Numerics.Pi;
   Angle_Degrees : Float := 45.0;
   Angle_Radians : Float := Ada.Numerics.Pi / 4.0;

   output : Float;
begin

   output := Sin (Angle_Degrees, Degrees_Cycle);
   Put (output); 
   output := Cos (Angle_Radians, Radians_Cycle);
   Put (output);
end Trig;
