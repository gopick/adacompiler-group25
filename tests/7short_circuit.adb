 with Ada.Text_IO;
with Ada.Integer_Text_IO;
use Ada.Integer_Text_IO;
use Ada.Text_IO;

procedure Test_shortcircuit is

   n : integer;
   m : integer;
   
begin
	Put_Line("Enter two numbers: ");
	Get(n);
	Get(m);
	if (n > 0 or m > 0) then
		Put_Line("One of the numbers is positive");
	else
		Put_Line("Both are negative");
	end if;

end Test_shortcircuit;
