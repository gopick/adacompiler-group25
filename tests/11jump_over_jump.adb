with Ada.Text_IO;
with Ada.Integer_Text_IO;
use Ada.Integer_Text_IO;
use Ada.Text_IO;
 
procedure jump is
   output : integer;
   n : integer;

begin
	Put_Line("Enter a positive number");
	Get(n);
	if (n>10) then
	   if(n>0) then
		n := n *10;
	   else
		n := n - 10;
	   end if;
	end if;
	Put(n);
end jump;
