with Ada.Text_IO;
with Ada.Integer_Text_IO;
use Ada.Integer_Text_IO;
use Ada.Text_IO;
 
procedure Test_Ackermann is
   outp : integer;
   n : integer;
   m : integer;

   procedure Ackermann (M: in integer;N : in integer; output : out integer) is
   temp : integer;
   temp_out : integer;
   begin
      if M = 0 then
         output := N + 1;
      elsif N = 0 then
         Ackermann (M - 1, 1,temp_out);
	 output := temp_out;
      else
	 Ackermann (M, N - 1,temp);
         Ackermann (M - 1, temp,temp_out);
         output := temp_out;
      end if;
   end Ackermann;

begin
	Put_Line("Enter two positive numbers");
	Get (m);
	Get (n);
	Ackermann(m,n,outp);
	Put(outp);

end Test_Ackermann;
