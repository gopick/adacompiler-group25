with Ada.Text_IO;
with Ada.Integer_Text_IO;
use Ada.Integer_Text_IO;
use Ada.Text_IO;

--TO TEST
-- 1 2 0           1 1 2         5 3 4
-- 0 1 1       *   2 1 1      =  3 3 2
-- 2 0 1           1 2 1         3 4 5

procedure matrix_mul is

   mat1 : array (1..3,1..3) of integer;
   mat2 : array (1..3,1..3) of integer;
   output : array (1..3,1..3) of integer;
   sum : integer;
   inGet : integer;
begin

	for i in 1..3 loop
	    for j in 1..3 loop
	    Put_Line("Enter a number into matrix1, row wise: ");
	    Get(inGet);
	    mat1(i,j) := inGet;
	    end loop;
        end loop;

	for i in 1..3 loop
	    for j in 1..3 loop
	    Put_Line("Enter a number into matrix2, row wise: ");
	    Get(inGet);
	    mat2(i,j) := inGet;
	    end loop;
        end loop;
	
	for i in 1..3 loop
	    for j in 1..3 loop
		sum := 0;
		for k in 1..3 loop
		    sum := sum + mat1(i,k) * mat2(k,j);
		end loop;
		output(i,j) := sum;
	    end loop;
	end loop;
	for i in 1..3 loop
	    for j in 1..3 loop
	    Put(output(i,j));
        Put_Line(" ");
	    end loop;
	New_Line(1);
        end loop;
end matrix_mul;

