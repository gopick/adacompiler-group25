with Ada.Text_IO;
with Ada.Integer_Text_IO;
use Ada.Integer_Text_IO;
use Ada.Text_IO;
 
procedure Test_Ackermann is
   n : integer;
   m : integer;

begin
	Put_Line("Enter two positive numbers");
	Get(n);
	Get(m) ;      
	Put_Line ("Negation of first number is");
	Put(-n);
	Put_Line (" First number subtracted from second is");
	Put(m-n);

end Test_Ackermann;
