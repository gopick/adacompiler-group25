with Ada.Text_IO;
with Ada.Integer_Text_IO;
use Ada.Integer_Text_IO;
use Ada.Text_IO;

procedure manyfunc is

	i : integer;
	arr : array (0..7) of integer;
	input : integer;

	procedure sum (a : in integer; b : in integer; c : in integer; d : in integer; e : in integer; f : in integer; g : in integer; h : in integer; output : out integer) is 
	begin
   	output := a+b+c+d+e+f+g+h;
	end sum;

begin

	Put_Line("Enter the numbers you want to add");
	Put_Line("Enter a number");
	Get(input);
	arr(0) := input;
	Put_Line("Enter a number");
	Get(input);
	arr(1) := input;
	Put_Line("Enter a number");
	Get(input);
	arr(2) := input;
	Put_Line("Enter a number");
	Get(input);
	arr(3) := input;
	Put_Line("Enter a number");
	Get(input);
	arr(4) := input;
	Put_Line("Enter a number");
	Get(input);
	arr(5) := input;
	Put_Line("Enter a number");
	Get(input);
	arr(6) := input;
	Put_Line("Enter a number");
	Get(input);
	arr(7) := input;
	
	sum(arr(0),arr(1),arr(2),arr(3),arr(4),arr(5),arr(6),arr(7),i);
	Put(i);

end manyfunc;


