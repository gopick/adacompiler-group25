with Ada.Text_IO;
with Ada.Integer_Text_IO;
use Ada.Integer_Text_IO;
use Ada.Text_IO;
with Ada.Float_Text_IO;
use Ada.Float_Text_IO;
procedure newton_raphson is
  

    --- function taken to be n*n*n -n - 1.0
    --Initial Guess : 1.5
    --Output should be 1.32 ()
    
   epi : float := 0.05;
   prog_output : float;
   init : float;
   yout : float;
   yderv : float;
   n : float  := 1.5;
		
begin
    Put_Line("The Function is: x^3 - x - 1");
    New_Line(1);
    Put_Line("Initial Guess: ");
    Put(n);
    Put_Line("Output(Root of the function): ");

	init := n;
    yout := n*n*n - n -1.0;
    yderv := 3.0*n*n -1.0;
	prog_output := init - yout/yderv;
	while ( init - prog_output >= epi) loop
        init := prog_output;
        yout := init*init*init - init -1.0;
        yderv := 3.0*init*init -1.0;
	    prog_output := init - yout/yderv;
	end loop;
	
	Put(prog_output);

end newton_raphson;
