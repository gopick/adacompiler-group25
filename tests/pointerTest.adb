with Ada.Text_IO;
with Ada.Integer_Text_IO;
use Ada.Integer_Text_IO;
use Ada.Text_IO;

procedure mutual_recursion is

   n : integer;
   m : integer;
   var2 : access integer;

begin
    Get(n);
    Get(m);
    var2 := n'Access;
    var2.all := m;
    Put(n);


end mutual_recursion;
