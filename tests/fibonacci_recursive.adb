with Ada.Text_IO;
with Ada.Integer_Text_IO;
use Ada.Integer_Text_IO;
use Ada.Text_IO;
 
procedure Fibonacci is
      n : integer;
      output: integer;
      procedure Fib (N:in Integer; outp:out integer)  is
          temp, temp2 : integer ;
      begin
         if N<3 then
            outp :=1 ;
         else 
            Fib(N-1, temp);
            Fib(N-2, temp2);
            outp := temp + temp2;
         end if;
      end Fib;
   
begin
      Put_Line("Enter the position of the fibonacci number you want");
      Get(n);
      Fib(n,output);
      Put(output);
end Fibonacci;
