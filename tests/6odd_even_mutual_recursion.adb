with Ada.Text_IO;
with Ada.Integer_Text_IO;
use Ada.Integer_Text_IO;
use Ada.Text_IO;

procedure mutual_recursion is
   output : integer;
   n : integer;
   procedure even (M: in integer; output1 : out integer);
   procedure odd (N: in integer; output2 : out integer) is
	begin
	    if N = 0 then
		output2 := 0;
	    else
		even( N-1, output2);
	    end if;
	end odd;

   procedure even (M: in integer; output1 : out integer) is
	begin
	    if M = 0 then
		output1 := 1;
	    else
		odd( M-1, output1);
	    end if;
	end even;

begin
	Put_Line("Enter a number you want to check whether it's even or odd");
	Get (n);
	even(n,output);
	if output = 0 then
	   Put_Line("It's odd");
        else
	   Put_Line("It's even");
	end if;

end mutual_recursion;
