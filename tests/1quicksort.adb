with Ada.Text_IO;
with Ada.Integer_Text_IO;
use Ada.Integer_Text_IO;
use Ada.Text_IO;

procedure quick_sort is
   arr : array (1..5) of integer;
   len : integer;
   inGet : integer;

   procedure partition (low : in integer; high: in integer; output : out integer) is
   pivot : integer;  
   i : integer; --smallest element
   swap : integer; -- to swap
   begin
	pivot := arr(high);
	i := low-1;
	
	for j in low..high-1 loop
	    if( arr(j) <= pivot) then
		i := i+1;
		swap := arr(i);
		arr(i) := arr(j);
		arr(j) := swap;
	    end if;
	end loop;

	arr(high) := arr(i+1);
	arr(i+1) := pivot;
	output := i+1;
   end partition;
   
   procedure quicksort (init : in integer ; final : in integer) is
   temp : integer;
   begin
	if( init < final ) then
	    partition(init, final, temp);
	    quicksort(init, temp - 1);
	    quicksort(temp + 1, final);
	end if;
   end quicksort;

begin

	for i in 1..5 loop
	    Put_Line("Enter a number into the array");
	    Get(inGet);
        arr(i) := inGet;
        end loop;

        -- ideally n will the length of the array, since length function is not implemented
        len := 5;
        quicksort(1,5);
	Put_Line("sorted array is");
	for i in 1..5 loop
	    Put(arr(i));
        end loop;
	
end quick_sort;
