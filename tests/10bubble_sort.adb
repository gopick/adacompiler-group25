with Ada.Text_IO;
with Ada.Integer_Text_IO;
use Ada.Integer_Text_IO;
use Ada.Text_IO;

 
procedure Bubble_sort is
   arr : array (1..5) of integer;
   output : integer;
   temp : integer;
   swapped : integer := 1;
   n : integer;
   flag : integer;
   inGet : integer;
begin

	for i in 1..5 loop
	    Put_Line("Enter a number into the array");
	    Get(inGet);
        arr(i) := inGet;
        end loop;

        -- ideally n will the length of the array, since length function is not implemented
        n := 5;
	while (swapped = 1) loop
	flag := 0;
	for i in 2..n loop
	    if ( arr(i-1) > arr(i) ) then 
		temp := arr(i-1);
		arr(i-1) := arr(i);
		arr(i) := temp;
		flag := 1;
	    end if;
        end loop;
 	if (flag = 0) then
		swapped := 0;
	end if;
	    n := n-1;
	end loop;

	Put_Line("sorted array is");
        for i in 1..5 loop
	    Put(arr(i));
	    New_Line(1);
	end loop;
end Bubble_sort;
