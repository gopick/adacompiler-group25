#!/usr/bin/env python
import sys
import lex
import yacc
import re
import sys
from  adaTokens import *
from symTable import *
from copy import deepcopy
# import parser
#This has been automatically generated using http://www.adaic.org/resources/add_content/standards/95lrm/grammar9x.y

curSymTable = curTable()
Debug1 = False

astFile = open('../tests/final.dot','w')
astFile.write("digraph Parse_tree {\n")

curStmtNo = 0

def opname(opstring):
    # Comparison
    oprel = {'>=_int':'ge_i', '<=_int':'le_i','>=_float':'ge_f','<=_float':'le_f','=_int':'e_i','=_float':'e_f'}

    # Operational
    oprel['+int'] = 'plus_int'
    oprel['-int'] = 'minus_int'
    oprel['+float'] = 'plus_float'
    oprel['-float'] = 'minus_float'
    oprel['*'] = 'mult'

    if opstring in oprel:
        return oprel[opstring]
    else:
        return opstring


def drawExpress(expres, head, i , j):

    ## i = statement_id
    ## j = expression_id

    if 'isCall' in expres:

        astFile.write("\t" +  expres['isCall']+str(i)+str(j) + '[ label =' + '\"' + expres['isCall']  + '\"''];\n')
        astFile.write("\t" + head  + "->" + expres['isCall']+str(i)+str(j) + ";\n")

        if expres['values'] != None:
            t = 1
            for item in expres['values']:
                drawExpress(item, expres['isCall']+str(i)+str(j), j ,t )
                t += 1
        return

    if 'name' in expres:
        astFile.write("\t" +  expres['name']+str(i)+str(j) + '[ label =' + '\"' + expres['name']  + '\"''];\n')
        astFile.write("\t" + head  + "->" + expres['name']+str(i)+str(j) + ";\n")
        return

    if 'isliteral' in expres:
        if expres['type'] == 'float':
            fname = 'v'+'f'+str(int(expres['val']))+str(i)+str(j)
        elif expres['type'] == 'char':
            fname = 'v'+'c'+expres['val'][1]+str(i)+str(j)
        else:
            fname = 'v'+str(expres['val'])+str(i)+str(j)
        astFile.write("\t" +  fname + '[ label =' + '\"' + str(expres['val'])  + '\"''];\n')
        astFile.write("\t" + head  + "->" + fname + ";\n")
        return

    opdisp = opname(expres['operator'])
    astFile.write("\t" +  'o'+opdisp+str(i)+str(j) + '[ label =' + '\"' + expres['operator']  + '\"''];\n')
    astFile.write("\t" + head  + "->" +  'o'+opdisp+str(i)+str(j) + ";\n")
    head =  'o'+opdisp+str(i)+str(j)


    drawExpress(expres['operand1'], head, i, j+1)
    drawExpress(expres['operand2'], head, i, j+2)
    return

def drawStmt(stmt, head):

    i = stmt['line']

    if stmt['stmt'] == 'assign':
        astFile.write("\t" + 'assign'+str(i) + '[ label =' + '\"' + '='  + '\"''];\n')
        astFile.write("\t" + head  + "->" + 'assign'+str(i) + ";\n")
        astFile.write("\t" + 'lhs'+str(i) + '[ label =' + '\"' + stmt['lhs']['name']  + '\"''];\n')
        astFile.write("\t" + 'assign'+str(i)  + "->" + 'lhs'+str(i) + ";\n")

        drawExpress(stmt['rhs'], 'assign'+str(i), i, 1)

    elif stmt['stmt'] == 'if':
        astFile.write("\t" + 'if_stmt'+str(i) + '[ label =' + '\"' + 'if_stmt'  + '\"''];\n')
        astFile.write("\t" + head  + "->" + 'if_stmt'+str(i) + ";\n")

        astFile.write("\t" + 'if'+str(i) + '[ label =' + '\"' + 'IF'  + '\"''];\n')
        astFile.write("\t" + 'if_stmt'+str(i)  + "->" + 'if'+str(i) + ";\n")

        expres = stmt['conditions'][0]['condition']
        astFile.write("\t" + 'condif_stmt'+str(i) + '[ label =' + '\"' + 'condition'  + '\"''];\n')
        astFile.write("\t" + 'if'+str(i)  + "->" + 'condif_stmt'+str(i) + ";\n")

        drawExpress(expres, 'condif_stmt'+str(i), 2000+i,2000)

        body = stmt['conditions'][0]['body']
        astFile.write("\t" + 'bodyif_stmt'+str(i) + '[ label =' + '\"' + 'body'  + '\"''];\n')
        astFile.write("\t" + 'if'+str(i)  + "->" + 'bodyif_stmt'+str(i) + ";\n")

        for stat in body:
            drawStmt(stat, 'bodyif_stmt'+str(i))


        if len(stmt['conditions']) > 1:
            for j in range(1,len(stmt['conditions'])):
                astFile.write("\t" + 'elsif_stmt'+str(i)+str(j) + '[ label =' + '\"' + 'else_if'  + '\"''];\n')
                astFile.write("\t" + 'if_stmt'+str(i)  + "->" + 'elsif_stmt'+str(i)+str(j) + ";\n")

                expres = stmt['conditions'][j]['condition']
                astFile.write("\t" + 'condelsif_stmt'+str(i)+str(j) + '[ label =' + '\"' + 'condition'  + '\"''];\n')
                astFile.write("\t" + 'elsif_stmt'+str(i)+str(j)  + "->" + 'condelsif_stmt'+str(i)+str(j) + ";\n")

                drawExpress(expres, 'condelsif_stmt'+str(i)+str(j), 6000+i+j,6000)

                body = stmt['conditions'][j]['body']
                astFile.write("\t" + 'bodyelsif_stmt'+str(i)+str(j) + '[ label =' + '\"' + 'body'  + '\"''];\n')
                astFile.write("\t" + 'elsif_stmt'+str(i) +str(j) + "->" + 'bodyelsif_stmt'+str(i)+str(j) + ";\n")

                for stat in body:
                    drawStmt(stat, 'bodyelsif_stmt'+str(i)+str(j))

        if stmt['else'] != None:
            astFile.write("\t" + 'else'+str(i) + '[ label =' + '\"' + 'else'  + '\"''];\n')
            astFile.write("\t" + 'if_stmt'+str(i)  + "->" + 'else'+str(i) + ";\n")

            for stat in stmt['else']:
                drawStmt(stat, 'else'+str(i))

    elif stmt['stmt'] == 'while' :

        astFile.write("\t" + 'while'+str(i) + '[ label =' + '\"' + 'while'  + '\"''];\n')
        astFile.write("\t" + head  + "->" + 'while'+str(i) + ";\n")


        astFile.write("\t" + 'cond'+str(i) + '[ label =' + '\"' + 'Condition'  + '\"''];\n')
        astFile.write("\t" + 'while'+str(i)  + "->" + 'cond'+str(i) + ";\n")

        drawExpress(stmt['cond'],'cond'+str(i),stmt['line'],8000)

        astFile.write("\t" + 'body'+str(i) + '[ label =' + '\"' + 'Body'  + '\"''];\n')
        astFile.write("\t" + 'while'+str(i)  + "->" + 'body'+str(i) + ";\n")

        for stat in stmt['body']:
            drawStmt(stat, 'body'+str(i))

    elif stmt['stmt'] == 'for':

        astFile.write("\t" + 'for'+str(i) + '[ label =' + '\"' + 'for'  + '\"''];\n')
        astFile.write("\t" + head  + "->" + 'for'+str(i) + ";\n")

        astFile.write("\t" + 'cond'+str(i) + '[ label =' + '\"' + 'Condition'  + '\"''];\n')
        astFile.write("\t" + 'for'+str(i)  + "->" + 'cond'+str(i) + ";\n")

        astFile.write("\t" + 'name'+str(i) + '[ label =' + '\"' + stmt['var']  + '\"''];\n')
        astFile.write("\t" + 'cond'+str(i)  + "->" + 'name'+str(i) + ";\n")

        astFile.write("\t" + 'range'+str(i) + '[ label =' + '\"' + 'Range'  + '\"''];\n')
        astFile.write("\t" + 'cond'+str(i)  + "->" + 'range'+str(i) + ";\n")

        if 'isliteral' in stmt['cond']['startVal']:
            astFile.write("\t" + 'left'+str(i) + '[ label =' + '\"' + str(stmt['cond']['startVal']['val']) + '\"''];\n')
        else:
            astFile.write("\t" + 'left'+str(i) + '[ label =' + '\"' + stmt['cond']['startVal']['name'] + '\"''];\n')

        if 'isliteral' in stmt['cond']['endVal']:
            astFile.write("\t" + 'right'+str(i) + '[ label =' + '\"' + str(stmt['cond']['endVal']['val']) + '\"''];\n')
        else:
            astFile.write("\t" + 'right'+str(i) + '[ label =' + '\"' + stmt['cond']['endVal']['name'] + '\"''];\n')


        astFile.write("\t" + 'range'+str(i)  + "->" + 'left'+str(i) + ";\n")
        astFile.write("\t" + 'range'+str(i)  + "->" + 'right'+str(i) + ";\n")

        astFile.write("\t" + 'body'+str(i) + '[ label =' + '\"' + 'Body'  + '\"''];\n')
        astFile.write("\t" + 'for'+str(i)  + "->" + 'body'+str(i) + ";\n")

        for stat in stmt['body']:
            drawStmt(stat, 'body'+str(i))

    elif stmt['stmt'] == 'return':

        astFile.write("\t" + 'return'+str(i) + '[ label =' + '\"' + 'return'  + '\"''];\n')
        astFile.write("\t" + head  + "->" + 'return'+str(i) + ";\n")

        if stmt['isVoid'] == False:
            drawExpress(stmt['expres'], 'return'+str(i), i, 1)

    elif stmt['stmt'] == 'procedure_call':

        astFile.write("\t" + 'proc_call'+str(i) + '[ label =' + '\"' + stmt['name']  + '\"''];\n')
        astFile.write("\t" + head  + "->" + 'proc_call'+str(i) + ";\n")

        if stmt['values'] != None:
            t = 1
            for item in stmt['values']:
                drawExpress(item, 'proc_call'+str(i), i ,t )
                t += 1

    else:
        print "Invalid Statement/Not Handled Yet"
        assert(1==2)

def drawDeclStmt(stmt, head ):

    if stmt['stmt'] == 'decl':
        i = stmt['line']
        declaration = stmt
        for var in declaration['vars']:
            astFile.write("\t" + var+str(i) + '[ label =' + '\"' + var  + '\"''];\n')
            astFile.write("\t" + head  + "->" + var+str(i) + ";\n")

            for attribute in declaration['attributes']:
                if attribute == 'name':
                    continue
                if attribute == 'expression':
                    drawExpress(declaration['attributes'][attribute],var+str(i), 1000+i, 1000 )
                    continue
                if attribute == 'val' and declaration['attributes']['type'] == 'float':
                     astFile.write("\t" + var +str(i)+'f'+ str(int(declaration['attributes'][attribute]))  + '[ label =' + '\"' + str(declaration['attributes'][attribute])  + '\"''];\n')
                     astFile.write("\t"+var+str(i)+"->"+ var+str(i)+'f'+ str(int(declaration['attributes'][attribute]))   + ";\n")
                     continue
                astFile.write("\t" + var+str(i) + str(declaration['attributes'][attribute])  + '[ label =' + '\"' + str(declaration['attributes'][attribute])  + '\"''];\n')
                astFile.write("\t" + var+str(i)  + "->" + var +str(i)+ str(declaration['attributes'][attribute])   + ";\n")

            i += 1

    elif stmt['stmt'] == 'function':

        astFile.write("\t" + 'func'+str(stmt['line']) + '[ label =' + '\"' + 'Function'  + '\"''];\n')
        astFile.write("\t" + head   + "->" + 'func'+ str(stmt['line']) + ";\n")

        astFile.write("\t" + 'function'+str(stmt['line']) + '[ label =' + '\"' + stmt['function']  + '\"''];\n')
        astFile.write("\t" +  'func'+ str(stmt['line'])   + "->" + 'function'+ str(stmt['line']) + ";\n")

        astFile.write("\t" + 'decl' + str(stmt['line'])+ '[ label =' + '\"' + 'decl'  + '\"''];\n')
        astFile.write("\t" + 'function'+str(stmt['line'])   + "->" + 'decl'+ str(stmt['line']) + ";\n")

        astFile.write("\t" + 'body'+str(stmt['line'])  + '[ label =' + '\"' + 'body'  + '\"''];\n')
        astFile.write("\t" + 'function'+str(stmt['line'])   + "->" + 'body'+str(stmt['line'])  + ";\n")

        ### Declarations
        if stmt['decl'] != None:
            for decl_stmt in stmt['decl']:
                drawDeclStmt(decl_stmt, 'decl'+str(stmt['line']) )

        ### Body
        for stat in stmt['body']:
            drawStmt(stat,'body'+str(stmt['line']))

        if 'returnType' in stmt:
            astFile.write("\t" + 'returnType' + str(stmt['line'])+ '[ label =' + '\"' + 'Return Type'  + '\"''];\n')
            astFile.write("\t" + 'function'+str(stmt['line'])   + "->" + 'returnType'+ str(stmt['line']) + ";\n")
            astFile.write("\t" + 'returnTypeVal' + str(stmt['line'])+ '[ label =' + '\"' + stmt['returnType']  + '\"''];\n')
            astFile.write("\t" + 'returnType'+str(stmt['line'])   + "->" + 'returnTypeVal'+ str(stmt['line']) + ";\n")

        if 'params' in stmt:
            astFile.write("\t" + 'params' + str(stmt['line'])+ '[ label =' + '\"' + 'Parameters'  + '\"''];\n')
            astFile.write("\t" + 'function'+str(stmt['line'])   + "->" + 'params'+ str(stmt['line']) + ";\n")

            for param in stmt['params']:
                param['line'] = stmt['line']
                param['stmt'] = 'decl'
                drawDeclStmt(param, 'params'+str(stmt['line']) )

    elif stmt['stmt'] == 'procedure':

        astFile.write("\t" + 'proc'+str(stmt['line']) + '[ label =' + '\"' + 'Procedure'  + '\"''];\n')
        astFile.write("\t" + head   + "->" + 'proc'+ str(stmt['line']) + ";\n")

        astFile.write("\t" + 'procedure'+str(stmt['line']) + '[ label =' + '\"' + stmt['procedure']  + '\"''];\n')
        astFile.write("\t" + 'proc'+ str(stmt['line']) + "->" + 'procedure'+ str(stmt['line']) + ";\n")

        astFile.write("\t" + 'decl' + str(stmt['line'])+ '[ label =' + '\"' + 'decl'  + '\"''];\n')
        astFile.write("\t" + 'procedure'+str(stmt['line'])   + "->" + 'decl'+ str(stmt['line']) + ";\n")

        astFile.write("\t" + 'body'+str(stmt['line'])  + '[ label =' + '\"' + 'body'  + '\"''];\n')
        astFile.write("\t" + 'procedure'+str(stmt['line'])   + "->" + 'body'+str(stmt['line'])  + ";\n")

        ### Declarations

        if stmt['decl'] != None:
            for decl_stmt in stmt['decl']:
                drawDeclStmt(decl_stmt, 'decl'+str(stmt['line']) )

        ### Body
        for stat in stmt['body']:
            drawStmt(stat,'body'+str(stmt['line']))

        if 'params' in stmt:
            astFile.write("\t" + 'params' + str(stmt['line'])+ '[ label =' + '\"' + 'Parameters'  + '\"''];\n')
            astFile.write("\t" + 'procedure'+str(stmt['line'])   + "->" + 'params'+ str(stmt['line']) + ";\n")

            for param in stmt['params']:
                param['line'] = stmt['line']
                param['stmt'] = 'decl'
                drawDeclStmt(param, 'params'+str(stmt['line']) )

    else:
        print "Invalid Decl Statement/Not Handled Yet"
        assert(1==2)


def p_start_symbol(p):
	'''start_symbol : compilation
    	'''
	p[0]=p[1]
        curSymTable.printTable()

        funcCSV = open('../tests/symTables/'+curSymTable.getName()+'.csv','w')
        curSymTable.printCSV(funcCSV)
        funcCSV.close()

	if (Debug1) : print "Rule Declared: 1"

def p_pragma(p):
	'''pragma  : PRAGMA IDENTIFIER ';'
	   | PRAGMA simple_name '(' pragma_arg_s ')' ';'
	'''
	if (Debug1) : print "Rule Declared: 2"


def p_pragma_arg_s(p):
	'''pragma_arg_s : pragma_arg
	   | pragma_arg_s ',' pragma_arg
	'''
	if (Debug1) : print "Rule Declared: 3"


def p_pragma_arg(p):
	'''pragma_arg : expression
	   | simple_name ARROW expression
	'''
	if (Debug1) : print "Rule Declared: 4"


def p_pragma_s(p):
	'''pragma_s :
	   | pragma_s pragma
	'''
	if (Debug1) : print "Rule Declared: 5"


def p_decl(p):
	'''decl : object_decl
	   | number_decl
	   | type_decl
	   | subtype_decl
	   | subprog_decl
	   | pkg_decl
	   | task_decl
	   | prot_decl
	   | exception_decl
	   | rename_decl
	   | generic_decl
	   | body_stub
	   | error ';'
	'''
        global curStmtNo
        p[0] = deepcopy(p[1])
        p[0]['stmt'] = 'decl'
        curStmtNo += 1
        p[0]['line'] = curStmtNo
	if (Debug1) : print "Rule Declared: 6"


def p_object_decl(p):
	'''object_decl : def_id_s ':' object_qualifier_opt object_subtype_def init_opt ';'
	'''
	if (Debug1) : print "Rule Declared: 7"

        if p[5] is not None and'type' in p[5]:

            # Type Check
            if p[4]['type'] != p[5]['type'] :
                print "Type mismatch in Object Declaration at Line: ",p.lineno(1)
                p_error(p)
                assert(1==2)
            else:
                if 'isliteral' in p[5]:
                    attributes = {'type': p[4]['type'], 'val': p[5]['val'] }
                else:
                    attributes = {'type': p[4]['type'] , 'expression':p[5] }

        else:
            attributes = {'type':p[4]['type']}

        p[0] = {'vars': p[1], 'attributes': attributes}
        for item in p[1]:
            attributes['name']= item
            curSymTable.newSym(item, attributes)



def p_def_id_s(p):
	'''def_id_s : def_id
	   | def_id_s ',' def_id
	'''
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[3]]

	if (Debug1) : print "Rule Declared: 8"


def p_def_id(p):
	'''def_id  : IDENTIFIER
	'''
        p[0] = p[1]
	if (Debug1) : print "Rule Declared: 9"


def p_object_qualifier_opt(p):
	'''object_qualifier_opt :
	   | ALIASED
	   | CONSTANT
	   | ALIASED CONSTANT
           | ACCESS
	'''
	if (Debug1) : print "Rule Declared: 10"


def p_object_subtype_def(p):
	'''object_subtype_def : subtype_ind
	   | array_type
	'''
        p[0] = p[1]
	if (Debug1) : print "Rule Declared: 11"


def p_init_opt(p):
	'''init_opt :
	   | ASSIGNMENT expression
	'''

        if len(p) == 3:
            p[0] = p[2]
        else:
            p[0] = None
        if (Debug1) : print "Rule Declared: 12"


def p_number_decl(p):
	'''number_decl : def_id_s ':' CONSTANT ASSIGNMENT expression ';'
	'''
        attributes = {'type':p[5]['type'],'val':p[5] }
        for item in p[1]:
            curSymTable.newSym(item,attributes)

        p[0] = deepcopy(p[5])

	if (Debug1) : print "Rule Declared: 13"


def p_type_decl(p):
	'''type_decl : TYPE IDENTIFIER discrim_part_opt type_completion ';'
	'''
        if p[2] not in reserved:
            curSymTable.newSym(p[2],p[4])
            p[0] = p[2]
        else:
            print "Reserved Identifier Used \n"
            p_error(p)

	if (Debug1) : print "Rule Declared: 14"


def p_discrim_part_opt(p):
	'''discrim_part_opt :
	   | discrim_part
	   | '(' LESSMORE ')'
	'''
	if (Debug1) : print "Rule Declared: 15"


def p_type_completion(p):
	'''type_completion :
	   | IS type_def
	'''
        p[0] = p[2]
	if (Debug1) : print "Rule Declared: 16"


def p_type_def(p):
	'''type_def : enumeration_type
	   | integer_type
	   | real_type
	   | array_type
	   | record_type
	   | access_type
	   | derived_type
	   | private_type
	'''
        p[0] = deepcopy(p[1])
	if (Debug1) : print "Rule Declared: 17"


def p_subtype_decl(p):
	'''subtype_decl : SUBTYPE IDENTIFIER IS subtype_ind ';'
	'''

        if p[2] not in reserved:
            curSymTable.newSym(p[2],p[4])
            p[0] = p[4]
        else:
            print "Reserved Name Used\n"
            p_error(p)

	if (Debug1) : print "Rule Declared: 18"


def p_subtype_ind(p):
	'''subtype_ind : name constraint
	   | name
	'''
        # subtype_ind (p[0]) should have 'type' attribute


        if len(p) == 2:
            if p[1]['name'] == 'integer':
                p[0] = {'type' : 'int' }
            else:
                p[0] = {'type' : p[1]['name'] }

        else:
            if p[1]['name'] == 'int' or p[1]['name'] == 'float':
                p[0] = deepcopy(p[2])
                p[0]['type']= p[1]['name']
            else:
                inSym = curSymTable.getSym(p[1]['name'])
                if inSym == None:
                    print "Undefined Type: ",p[1]['name']
                    p_error(p)
                else:
                    ### Delta and digits not handled for both name and constraint
                    if p[2]['startVal'] >= inSym['startVal'] and p[2]['endVal'] <= inSym['endVal']:
                        p[0] = deepcopy(inSym)
                        p[0]['startVal'] = p[2]['startVal']
                        p[0]['endVal'] = p[2]['endVal']
                    else:
                        print "Conflicting ranges of type and subtype\n"
                        p_error(p)


	if (Debug1) : print "Rule Declared: 19"


def p_constraint(p):
	'''constraint : range_constraint
	   | decimal_digits_constraint
	'''
        p[0] = p[1]
	if (Debug1) : print "Rule Declared: 20"


def p_decimal_digits_constraint(p):
	'''decimal_digits_constraint : DIGITS expression range_constr_opt
	'''
        if p[2]['type'] == 'int':
            p[0] = deepcopy(p[3])
            p[0]['digits'] = p[2]
        else:
            print "Invalid Digits\n"
            p_error(p)
	if (Debug1) : print "Rule Declared: 21"


def p_derived_type(p):
	'''derived_type : NEW subtype_ind
	   | NEW subtype_ind WITH PRIVATE
	   | NEW subtype_ind WITH record_def
	   | ABSTRACT NEW subtype_ind WITH PRIVATE
	   | ABSTRACT NEW subtype_ind WITH record_def
	'''
	if (Debug1) : print "Rule Declared: 22"


def p_range_constraint(p):
	'''range_constraint : RANGE range
	'''
        p[0] = p[2]
	if (Debug1) : print "Rule Declared: 23"


def p_range(p):
	'''range :  name TICK RANGE
	   | name TICK RANGE '(' expression ')'
	'''

	if (Debug1) : print "Rule Declared: 24"

def p_range1(p):
	'''range : simple_expression DOTDOT simple_expression
	'''

        p[0] = {'startVal' : p[1] , 'endVal': p[3] }
	if (Debug1) : print "Rule Declared: 24a"


def p_enumeration_type(p):
	'''enumeration_type : '(' enum_id_s ')'
	'''
        p[0] = {'val':p[2], 'type':'enum' }
	if (Debug1) : print "Rule Declared: 25"


def p_enum_id_s(p):
	'''enum_id_s : enum_id
	   | enum_id_s ',' enum_id
	'''
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[3]]
	if (Debug1) : print "Rule Declared: 26"


def p_enum_id(p):
	'''enum_id : IDENTIFIER
	   | CHAR
	'''
        p[0] = p[1]
	if (Debug1) : print "Rule Declared: 27"


def p_integer_type(p):
	'''integer_type : range_spec
	   | MOD expression
	'''
        if len(p) == 2:
            p[0] = deepcopy(p[1])
            p[0]['type'] = 'range'
        else:
            if p[2]['type'] == 'int':
                p[0] = deepcopy(p[2])
                p[0]['type'] = 'mod'
            else:
                print "Illegal Operand to MOD\n"
                p_error(p)

	if (Debug1) : print "Rule Declared: 28"


def p_(p):
	'''
	'''
	if (Debug1) : print "Rule Declared: 29"


def p_range_spec(p):
	'''range_spec : range_constraint
	'''
        p[0] = p[1]
	if (Debug1) : print "Rule Declared: 30"


def p_range_spec_opt(p):
	'''range_spec_opt :
	   | range_spec
	'''
        p[0] = p[1]
	if (Debug1) : print "Rule Declared: 31"


def p_real_type(p):
	'''real_type : float_type
	   | fixed_type
	'''
        p[0] = p[1]
	if (Debug1) : print "Rule Declared: 32"


def p_float_type(p):
	'''float_type : DIGITS expression range_spec_opt
	'''
        if p[2]['type'] == 'int':
            p[0] = deepcopy(p[3])
            p[0]['digits'] = p[2]
            p[0]['type'] = 'float_type'
        else:
            print "Digits not integer\n"
            p_error(p)
	if (Debug1) : print "Rule Declared: 33"


def p_fixed_type(p):
	'''fixed_type : DELTA expression range_spec
	   | DELTA expression DIGITS expression range_spec_opt
	'''
        if len(p) == 4:
            if p[2]['type'] == 'float' or p[2]['type'] == 'int' :
                p[0] = deepcopy(p[3])
                p[0]['delta'] = p[2]
                p[0]['type'] = 'fixed_type'
            else:
                print "Invalid Delta\n"
                p_error(p)
                p[0] = None
        else :
             if p[2]['type'] == 'float' or p[2]['type'] == 'int' :
                if p[4]['type'] == 'int':
                    p[0] = deepcopy(p[5])
                    p[0]['delta'] = p[2]
                    p[0]['type'] = 'fixed_type'
                    p[0]['digits'] = p[4]
                else:
                    print "Invalid Digits\n"
                    p_error(p)
                    p[0] = None
             else:
                print "Invalid Delta\n"
                p_error(p)
                p[0] = None


	if (Debug1) : print "Rule Declared: 34"


def p_array_type(p):
	'''array_type : unconstr_array_type
	   | constr_array_type
	'''
        p[0] = p[1]
	if (Debug1) : print "Rule Declared: 35"


def p_unconstr_array_type(p):
	'''unconstr_array_type : ARRAY '(' index_s ')' OF component_subtype_def
	'''
        ## TODO
	if (Debug1) : print "Rule Declared: 36"


def p_constr_array_type(p):
	'''constr_array_type : ARRAY iter_index_constraint OF component_subtype_def
	'''
        p[0] = {'size': p[2], 'dimension':len(p[2]), 'type': p[4]['type']}
	if (Debug1) : print "Rule Declared: 37"


def p_component_subtype_def(p):
	'''component_subtype_def : aliased_opt subtype_ind
	'''
        ## TODO aliased_opy
        p[0] = p[2]
	if (Debug1) : print "Rule Declared: 38"


def p_aliased_opt(p):
	'''aliased_opt :
	   | ALIASED
	'''
        # p[0] = p[1]
	if (Debug1) : print "Rule Declared: 39"


def p_index_s(p):
	'''index_s : index
	   | index_s ',' index
	'''
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[3]]
	if (Debug1) : print "Rule Declared: 40"


def p_index(p):
	'''index : name RANGE LESSMORE
	'''
	if (Debug1) : print "Rule Declared: 41"


def p_iter_index_constraint(p):
	'''iter_index_constraint : '(' iter_discrete_range_s ')'
	'''
        p[0] = p[2]
	if (Debug1) : print "Rule Declared: 42"


def p_iter_discrete_range_s(p):
	'''iter_discrete_range_s : discrete_range
	   | iter_discrete_range_s ',' discrete_range
	'''
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[3]]
	if (Debug1) : print "Rule Declared: 43"


def p_discrete_range(p):
	'''discrete_range : name range_constr_opt
	   | range
	'''
        if len(p) == 2:
            p[0] = p[1]
        else:
            print "Unhandled name range_constr_opt\n"
            p_error(p)
	if (Debug1) : print "Rule Declared: 44"


def p_range_constr_opt(p):
	'''range_constr_opt :
	   | range_constraint
	'''
        p[0] = p[1]
	if (Debug1) : print "Rule Declared: 45"


def p_record_type(p):
	'''record_type : tagged_opt limited_opt record_def
	'''
	if (Debug1) : print "Rule Declared: 46"


def p_record_def(p):
	'''record_def : RECORD pragma_s comp_list END RECORD
	   | NuLL RECORD
	'''
	if (Debug1) : print "Rule Declared: 47"


def p_tagged_opt(p):
	'''tagged_opt :
	   | TAGGED
	   | ABSTRACT TAGGED
	'''
	if (Debug1) : print "Rule Declared: 48"


def p_comp_list(p):
	'''comp_list : comp_decl_s variant_part_opt
	   | variant_part pragma_s
	   | NuLL ';' pragma_s
	'''
	if (Debug1) : print "Rule Declared: 49"


def p_comp_decl_s(p):
	'''comp_decl_s : comp_decl
	   | comp_decl_s pragma_s comp_decl
	'''
	if (Debug1) : print "Rule Declared: 50"


def p_variant_part_opt(p):
	'''variant_part_opt : pragma_s
	   | pragma_s variant_part pragma_s
	'''
	if (Debug1) : print "Rule Declared: 51"


def p_comp_decl(p):
	'''comp_decl : def_id_s ':' component_subtype_def init_opt ';'
	   | error ';'
	'''
	if (Debug1) : print "Rule Declared: 52"


def p_discrim_part(p):
	'''discrim_part : '(' discrim_spec_s ')'
	'''
	if (Debug1) : print "Rule Declared: 53"


def p_discrim_spec_s(p):
	'''discrim_spec_s : discrim_spec
	   | discrim_spec_s ';' discrim_spec
	'''
	if (Debug1) : print "Rule Declared: 54"


def p_discrim_spec(p):
	'''discrim_spec : def_id_s ':' access_opt mark init_opt
	   | error
	'''
	if (Debug1) : print "Rule Declared: 55"


def p_access_opt(p):
	'''access_opt :
	   | ACCESS
	'''
	if (Debug1) : print "Rule Declared: 56"


def p_variant_part(p):
	'''variant_part : CASE simple_name IS pragma_s variant_s END CASE ';'
	'''
	if (Debug1) : print "Rule Declared: 57"


def p_variant_s(p):
	'''variant_s : variant
	   | variant_s variant
	'''
	if (Debug1) : print "Rule Declared: 58"


def p_variant(p):
	'''variant : WHEN choice_s ARROW pragma_s comp_list
	'''
	if (Debug1) : print "Rule Declared: 59"


def p_choice_s(p):
	'''choice_s : choice
	   | choice_s '|' choice
	'''
	if (Debug1) : print "Rule Declared: 60"


def p_choice(p):
	'''choice : expression
	   | discrete_with_range
	   | OTHERS
	'''
	if (Debug1) : print "Rule Declared: 61"


def p_discrete_with_range(p):
	'''discrete_with_range : name range_constraint
	   | range
	'''

        if len(p) == 2:
            p[0] = deepcopy(p[1])
        else :
            print "Unhandled range_constraint\n"
            p_error(p)

	if (Debug1) : print "Rule Declared: 62"


def p_access_type(p):
	'''access_type : ACCESS subtype_ind
	   | ACCESS CONSTANT subtype_ind
	   | ACCESS ALL subtype_ind
	   | ACCESS prot_opt PROCEDURE formal_part_opt
	   | ACCESS prot_opt FUNCTION formal_part_opt RETURN mark
	'''
	if (Debug1) : print "Rule Declared: 63"


def p_prot_opt(p):
	'''prot_opt :
	   | PROTECTED
	'''
	if (Debug1) : print "Rule Declared: 64"


def p_decl_part(p):
	'''decl_part :
	   | decl_item_or_body_s1
	'''
        if len(p) == 1:
            p[0] = None
        else:
            p[0] = p[1]
	if (Debug1) : print "Rule Declared: 65"


def p_decl_item_s(p):
	'''decl_item_s :
	   | decl_item_s1
	'''
        p[0] = p[1]
	if (Debug1) : print "Rule Declared: 66"


def p_decl_item_s1(p):
	'''decl_item_s1 : decl_item
	   | decl_item_s1 decl_item
	'''
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[2]]

	if (Debug1) : print "Rule Declared: 67"


def p_decl_item(p):
	'''decl_item : decl
	   | use_clause
	   | rep_spec
	   | pragma
	'''
        p[0] = p[1]
	if (Debug1) : print "Rule Declared: 68"


def p_decl_item_or_body_s(p):
	'''decl_item_or_body_s1 : decl_item_or_body
	   | decl_item_or_body_s1 decl_item_or_body
	'''
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[2]]
	if (Debug1) : print "Rule Declared: 69"


def p_decl_item_or_body(p):
	'''decl_item_or_body : body
	   | decl_item
	'''
        p[0] = p[1]
	if (Debug1) : print "Rule Declared: 70"


def p_body(p):
	'''body : pkg_body
	   | task_body
	   | prot_body
	'''
        p[0] = p[1]
	if (Debug1) : print "Rule Declared: 71"

def p_body1(p):
	'''body : subprog_body
	'''
        global curSymTable
        p[0] = p[1]
        curSymTable = curSymTable.endScope()
        funcCSV = open('../tests/symTables/'+curSymTable.getName()+'.csv','w')
        curSymTable.printCSV(funcCSV)
        funcCSV.close()
	if (Debug1) : print "Rule Declared: 71a"


def p_name(p):
	'''name : simple_name
	   | indexed_comp
	   | selected_comp
	   | attribute
	   | operator_symbol
	'''
        p[0] = deepcopy(p[1])
	if (Debug1) : print "Rule Declared: 72"


def p_mark(p):
	'''mark : simple_name
	   | mark TICK attribute_id
	   | mark '.' simple_name
	'''
        if len(p) == 2:
            p[0] = p[1]
        else :
            print "Unhandled second and third rule for mark\n"
            p_error(p)
	if (Debug1) : print "Rule Declared: 73"


def p_simple_name(p):
	'''simple_name : IDENTIFIER
	'''

        if p[1] == 'true':
            p[0] = {'type': 'bool', 'val': 'true'}
        elif p[1] == 'false':
            p[0] = {'type': 'bool', 'val': 'false'}
        else:
            p[0] = {'name': p[1] }

	if (Debug1) : print "Rule Declared: 74"


def p_compound_name(p):
	'''compound_name : simple_name
	   | compound_name '.' simple_name
	'''
        if len(p) ==2 :
            p[0] = p[1]
	if (Debug1) : print "Rule Declared: 75"


def p_c_name_list(p):
	'''c_name_list : compound_name
	    | c_name_list ',' compound_name
	'''
	if (Debug1) : print "Rule Declared: 76"


def p_used_char(p):
	'''used_char : CHAR
	'''
        p[0] = p[1]
	if (Debug1) : print "Rule Declared: 77"


def p_operator_symbol(p):
	'''operator_symbol : STRING
	'''
        p[0] = p[1]
	if (Debug1) : print "Rule Declared: 78"


def p_indexed_comp(p):
	'''indexed_comp : name '(' value_s ')'
	'''
        func =  curSymTable.getSym(p[1]['name'] )
        if func == None:
            print "function not defined\n"
            p_error(p)
        else:
            if 'subprog' not in func:
                return "Not a Function Name\n"
                p_error(p)
            if func['subprog'] == 'function' or func['subprog'] == 'procedure':
                if 'params' in func and len(p[3]) != len(func['params']):
                    print "Number of Params Do NOT match\n"
                    p_error(p)
                elif 'params' not in func and len(p[3]) > 0:
                    print "params not expected\n"
                    p_error(p)
                elif func['subprog'] == 'function':
                    if 'params' in func:
                        i = 0
                        for item in p[3]:
                            if item['type'] != func['params'][i]['attributes']['type']:
                                print "Parameter Type Mismatch\n"
                                p_error(p)
                            i += 1
                    if 'returnType' not in func:
                        print "No Return Value"
                        p_error(p)
                    else:
                        p[0] = {'name':p[1]['name'],'isCall': p[1]['name'], 'values':p[3], 'type':func['returnType'] }
                elif func['subprog'] == 'procedure':
                    if 'params' in func:
                        i = 0
                        for item in p[3]:
                            if item['type'] != func['params'][i]['attributes']['type']:
                                print "Parameter Type Mismatch\n"
                                p_error(p)
                            elif func['params'][i]['attributes']['mode'].lower() == 'out' and 'isliteral' in item:
                                print "Out Parameter has to be a variable\n"
                                p_error(p)
                            i += 1
                    p[0] = {'name':p[1]['name'],'isCall': p[1]['name'], 'values':p[3] }





	if (Debug1) : print "Rule Declared: 79"


def p_value_s(p):
	'''value_s : value
	   | value_s ',' value
	'''
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[3]]

	if (Debug1) : print "Rule Declared: 80"


def p_value(p):
	'''value : expression
            | discrete_with_range
	'''
        p[0] = deepcopy(p[1])
	if (Debug1) : print "Rule Declared: 81"

def p_value2(p):
	'''value : error
	   | comp_assoc
	'''
        p[0] = []
        print "Unhandled comp_assoc/error"
	if (Debug1) : print "Rule Declared: 81b"



def p_selected_comp(p):
	'''selected_comp : name '.' simple_name
	   | name '.' used_char
	   | name '.' operator_symbol
	   | name '.' ALL
	'''
	if (Debug1) : print "Rule Declared: 82"


def p_attribute(p):
	'''attribute : name TICK attribute_id
	'''
	if (Debug1) : print "Rule Declared: 83"


def p_attribute_id(p):
	'''attribute_id : IDENTIFIER
	   | DIGITS
	   | DELTA
	   | ACCESS
	'''
	if (Debug1) : print "Rule Declared: 84"


def p_literal(p):
	'''literal : INTEGER
	'''
        p[0] = { 'val': p[1], 'type': 'int' }

	if (Debug1) : print "Rule Declared: 85"

def p_literal1(p):
	'''literal : BASE_INTEGER
	'''
        p[0] = { 'val': p[1], 'type': 'base_int' }
	if (Debug1) : print "Rule Declared: 85a"

def p_literal2(p):
	'''literal : FLOAT
	'''
        p[0] = { 'val': p[1], 'type': 'float' }
	if (Debug1) : print "Rule Declared: 85b"

def p_literal3(p):
	'''literal : BASE_FLOAT
	'''
        p[0] = { 'val': p[1], 'type': 'base_float' }
	if (Debug1) : print "Rule Declared: 85c"

def p_literal4(p):
	'''literal : used_char
	'''
        p[0] = { 'val': p[1], 'type': 'char' }
	if (Debug1) : print "Rule Declared: 85d"


def p_literal5(p):
	'''literal :  NuLL
	'''
        p[0] = { 'val': p[1], 'type': 'null' }
	if (Debug1) : print "Rule Declared: 85e"



def p_aggregate(p):
	'''aggregate : '(' comp_assoc ')'
	   | '(' value_s_2 ')'
	   | '(' expression WITH value_s ')'
	   | '(' expression WITH NuLL RECORD ')'
	   | '(' NuLL RECORD ')'
	'''
	if (Debug1) : print "Rule Declared: 86"


def p_value_s_(p):
	'''value_s_2 : value ',' value
	   | value_s_2 ',' value
	'''
	if (Debug1) : print "Rule Declared: 87"


def p_comp_assoc(p):
	'''comp_assoc : choice_s ARROW expression
	'''
	if (Debug1) : print "Rule Declared: 88"


def p_expression(p):
	'''expression : relation
	   | expression logical relation
	   | expression short_circuit relation
	'''
        if len(p) == 2:
            p[0] = p[1]
        else:
            if p[1]['type'] == 'bool':
                p[0] = {'operand1': p[1], 'operator':p[2], 'operand2': p[3], 'type': 'bool'}
            else:
                print "Illegal operands to ",p[2],"\n"
                p_error(p)

	if (Debug1) : print "Rule Declared: 89"


def p_logical(p):
	'''logical : AND
	   | OR
	   | XOR
	'''
        p[0] = p[1]
	if (Debug1) : print "Rule Declared: 90"


def p_short_circuit(p):
	'''short_circuit : AND THEN
	   | OR ELSE
	'''
        # Not handling and then / or else
        p[0] = p[1]
	if (Debug1) : print "Rule Declared: 91"


def p_relation(p):
	'''relation : simple_expression
	   | simple_expression membership range
	   | simple_expression membership name
	'''
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = {'operand1': p[1], 'operator':p[2], 'operand2':p[3], 'type':'bool' }
	if (Debug1) : print "Rule Declared: 92"

def p_relation1(p):
	'''relation : simple_expression relational simple_expression
	'''

        if p[1]['type'] != p[3]['type']:
            print "Illegal Operands to relational, Type Mismatch\n"
            p_error(p)
        else:
            if p[1]['type'] == 'float':
                p[0] = {'operand1': p[1], 'operator':str(p[2]) + '_float', 'operand2':p[3], 'type':'bool' }
            else:
                p[0] = {'operand1': p[1], 'operator':str(p[2]) + '_int', 'operand2':p[3], 'type':'bool' }

	if (Debug1) : print "Rule Declared: 92a"


def p_relational(p):
	'''relational : '='
	   | NOTEQUAL
	   | '<'
	   | LESSEQ
	   | '>'
	   | GREATEREQ
	'''
        p[0] = p[1]
	if (Debug1) : print "Rule Declared: 93"


def p_membership(p):
	'''membership : IN
	   | NOT IN
	'''
        p[0] = p[1]
	if (Debug1) : print "Rule Declared: 94"


def p_simple_expression(p):
	'''simple_expression : unary term
	   | term
	   | simple_expression adding term
	'''

        if len(p) == 2:
            p[0] = deepcopy(p[1])
        elif len(p) == 3:
            if p[1] == '-':
                if p[2]['type'] == 'char':
                    print "Error: Unary Operator with char Type"
                    p_error(p)
                    p[0] = None
                elif p[2]['type'] == 'int' or p[2]['type'] == 'float':
                    p[0] = {'operand1':-1, 'operator': '*' , 'operand2':p[2] ,'type': p[2]['type'] }
                else:
                    print "Error: Unary Operator with Illegal Type"
                    p_error(p)
                    p[0] = None
            else:
                p[0] = deepcopy(p[2])
        else :
            if p[2] == '+':

                if p[1]['type'] == p[3]['type'] and p[1]['type'] == 'int':
                    p[0] = {'operand1': p[1], 'operator': '+int', 'operand2':p[3], 'type':p[1]['type'] }
                elif p[1]['type'] == p[3]['type'] and p[1]['type'] == 'float':
                    p[0] = {'operand1': p[1], 'operator': '+float', 'operand2':p[3], 'type':p[1]['type'] }
                elif p[1]['type'] == 'float' and p[3]['type'] == 'int':
                    p[0] = {'operand1': p[1], 'operator': '+float', 'operand2':p[3], 'type':p[1]['type'] }
                elif p[1]['type'] == 'int' and p[3]['type'] == 'float':
                    p[0] = {'operand1': p[1], 'operator': '+float', 'operand2':p[3], 'type':p[3]['type'] }
                else:
                    print "Error: Addition Operation not possible, Illegal Operands to Operator"
                    p_error(p)
            elif p[2] == '-':

                if p[1]['type'] == p[3]['type'] and p[1]['type'] == 'int':
                    p[0] = {'operand1': p[1], 'operator': '-int', 'operand2':p[3], 'type':p[1]['type'] }
                elif p[1]['type'] == p[3]['type'] and p[1]['type'] == 'float':
                    p[0] = {'operand1': p[1], 'operator': '-float', 'operand2':p[3], 'type':p[1]['type'] }
                elif p[1]['type'] == 'float' and p[3]['type'] == 'int':
                    p[0] = {'operand1': p[1], 'operator': '-float', 'operand2':p[3], 'type':p[1]['type'] }
                elif p[1]['type'] == 'int' and p[3]['type'] == 'float':
                    p[0] = {'operand1': p[1], 'operator': '-float', 'operand2':p[3], 'type':p[3]['type'] }
                else:
                    print "Error: Subtraction Operation not possible, Illegal Operands to Operator"
                    p_error(p)
            elif p[2] == '&':

                if p[1]['type'] == p[3]['type'] and p[1]['type'] == 'string':
                    p[0] = {'operand1': p[1], 'operator': '&', 'operand2':p[3], 'type':p[1]['type'] }
                else:
                    print "Error: Concatation Operation not possible, Illegal Operands to Operator"
                    p_error(p)

	if (Debug1) : print "Rule Declared: 95"


def p_unary(p):
	'''unary   : '+'
	   | '-'
	'''
        p[0] = p[1]
	if (Debug1) : print "Rule Declared: 96"


def p_adding(p):
	'''adding  : '+'
	   | '-'
	   | '&'
	'''
        p[0] = p[1]
	if (Debug1) : print "Rule Declared: 97"


def p_term(p):
	'''term    : factor
	   | term multiplying factor
	'''

        allowed = ['int', 'float']

        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = {'operand1': p[1], 'operator': p[2], 'operand2': p[3] }

            if p[2] == '*' or p[2] == '/':

                if p[1]['type'] in allowed and p[3]['type'] in allowed:

                    if p[1]['type'] == 'float' or p[3]['type'] == 'float' :
                        p[0]['type'] = 'float'
                    else:
                        p[0]['type'] = 'int'
                else:
                    print "Illegal operands to: ",p[2],"\n"
                    p_error(p)
            elif p[1]['type'] in allowed or p[3]['type'] == 'int':
                p[0]['type'] = p[1]['type']
            else :
                print "Illegal operands to: ",p[2],"\n"
                p_error(p)

	if (Debug1) : print "Rule Declared: 98"


def p_multiplying(p):
	'''multiplying : '*'
	   | '/'
	   | MOD
	   | REM
	'''
        p[0] = p[1]
	if (Debug1) : print "Rule Declared: 99"


def p_factor(p):
	'''factor : primary
	   | NOT primary
	   | primary STARSTAR primary
	'''
        if len(p) == 2:
            p[0] = deepcopy(p[1])
        elif len(p) == 3:
            if p[2]['type'] == 'bool':
                p[0] = {'operand1': p[2], 'operator': p[1], 'type': 'bool'}
            else:
                print "Operand not boolean, Illegal operands to NOT\n"
                p_error(p)
        else:
            allowed = ['int', 'float']
            if p[1]['type'] in allowed and p[3]['type'] == 'int':
                p[0] = {'operand1': p[1], 'operator':p[2] , 'operand2':p[3], 'type':p[1]['type'] }
            else :
                print "Illegal Operands to **\n"
                p_error(p)
	if (Debug1) : print "Rule Declared: 100"

def p_factor1(p):
	'''factor : ABS primary
	'''
        allowed = ['int', 'float']
        if p[2]['type'] in allowed:
            p[0] = {'operand1': p[2] , 'operator': p[1], 'type':p[2]['type'] }
        else :
            print "Illegal Operands to ABS\n"
            p_error(p)
	if (Debug1) : print "Rule Declared: 100a"


def p_primary(p):
	'''primary : parenthesized_primary
	'''
        p[0] = p[1]

	if (Debug1) : print "Rule Declared: 101"

def p_primary1(p):
	'''primary : name
	'''

        if p[1] == None:
            p[0] = p[1]
            return

        if 'type' in p[1] and p[1]['type'] == 'bool':
            p[0] = p[1]

        else:

            if p[1]['name'] in reserved:
                print "Error: Reserved Name Used: ", p[1]['name'],"\n"
                p_error(p)
                p[0] = None
            elif 'isCall' in p[1]:
                p[0] = p[1]
            else:
                inSym = curSymTable.getSym(p[1]['name'])
                if inSym == None:
                    ## Error printed by getSym
                    p_error(p)
                    p[0] = None
                else:
                    p[0] = deepcopy(inSym)

	if (Debug1) : print "Rule Declared: 101a"

def p_primary2(p):
	'''primary : literal
	'''

        p[0] = deepcopy(p[1])
        p[0]['isliteral'] = True


	if (Debug1) : print "Rule Declared: 101b"

def p_primary3(p):
	'''primary : allocator
	   | qualified
	'''

        print "Unhandled allocator/qualified\n"
	if (Debug1) : print "Rule Declared: 101c"



def p_parenthesized_primary(p):
	'''parenthesized_primary : aggregate
	   | '(' expression ')'
	'''
        if len(p)==4:
            p[0] = p[2]
        else :
            print "Unhandled aggregate"
	if (Debug1) : print "Rule Declared: 102"


def p_qualified(p):
	'''qualified : name TICK parenthesized_primary
	'''
	if (Debug1) : print "Rule Declared: 103"


def p_allocator(p):
	'''allocator : NEW name
	   | NEW qualified
	'''
	if (Debug1) : print "Rule Declared: 104"


def p_statement_s(p):
	'''statement_s : statement
	   | statement_s statement
	'''
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1]+ [p[2]]


	if (Debug1) : print "Rule Declared: 105"


def p_statement(p):
	'''statement : unlabeled
	   | label statement
	'''
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = deepcopy(p[2])
            p[0]['label'] = p[1]

	if (Debug1) : print "Rule Declared: 106"


def p_unlabeled(p):
	'''unlabeled : simple_stmt
	   | compound_stmt
	   | pragma
	'''
        p[0] = p[1]
	if (Debug1) : print "Rule Declared: 107"


def p_simple_stmt(p):
	'''simple_stmt : null_stmt
	   | exit_stmt
	   | goto_stmt
	   | delay_stmt
	   | abort_stmt
	   | raise_stmt
	   | code_stmt
	   | requeue_stmt
	   | error ';'
	'''
	if (Debug1) : print "Rule Declared: 108"

def p_simple_stmt1(p):
	'''simple_stmt : assign_stmt
        '''
        global curStmtNo
        p[0] = deepcopy(p[1])
        p[0]['stmt'] = 'assign'
        curStmtNo +=1
        p[0]['line'] = curStmtNo
	if (Debug1) : print "Rule Declared: 108a"

def p_simple_stmt2(p):
	'''simple_stmt : return_stmt
        '''
        global curStmtNo
        p[0] = deepcopy(p[1])
        p[0]['stmt'] = 'return'
        curStmtNo +=1
        p[0]['line'] = curStmtNo
	if (Debug1) : print "Rule Declared: 108b"

def p_simple_stmt3(p):
	'''simple_stmt : procedure_call
	'''
        global curStmtNo
        p[0] = deepcopy(p[1])
        p[0]['stmt'] = 'procedure_call'
        curStmtNo +=1
        p[0]['line'] = curStmtNo
	if (Debug1) : print "Rule Declared: 108c"


def p_compound_stmt(p):
	'''compound_stmt : case_stmt
	   | block
	   | accept_stmt
	   | select_stmt
	'''
	if (Debug1) : print "Rule Declared: 109"

def p_compound_stmt1(p):
	'''compound_stmt : if_stmt
	'''
        global curStmtNo
        p[0] = deepcopy(p[1])
        p[0]['stmt'] = 'if'
        curStmtNo += 1
        p[0]['line'] = curStmtNo
	if (Debug1) : print "Rule Declared: 109b"

def p_compound_stmt2(p):
	'''compound_stmt : loop_stmt
	'''
        global curStmtNo
        p[0] = deepcopy(p[1])
        curStmtNo += 1
        p[0]['line'] = curStmtNo

	if (Debug1) : print "Rule Declared: 109c"



def p_label(p):
	'''label : LESSLESS IDENTIFIER MOREMORE
	'''
        p[0] = p[2]
	if (Debug1) : print "Rule Declared: 110"


def p_null_stmt(p):
	'''null_stmt : NuLL ';'
	'''
	if (Debug1) : print "Rule Declared: 111"


def p_assign_stmt(p):
	'''assign_stmt : name ASSIGNMENT expression ';'
	'''
        inSym = curSymTable.getSym(p[1]['name'])
        if inSym == None:
            print "Variable ",p[1]['name'],"used before declaration"
            p_error(p)
        else:
            if inSym['type'] == p[3]['type']:
                p[0] = {'lhs':p[1],'rhs':p[3]}
            else:
                print "Type Mismatch,",inSym['type']," vs ",p[3]['type']
                p_error(p)
	if (Debug1) : print "Rule Declared: 112"


def p_if_stmt(p):
	'''if_stmt : IF cond_clause_s else_opt END IF ';'
	'''
        p[0] = {'conditions':p[2], 'else':p[3]}
	if (Debug1) : print "Rule Declared: 113"


def p_cond_clause_s(p):
	'''cond_clause_s : cond_clause
	   | cond_clause_s ELSIF cond_clause
	'''
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[3]]
	if (Debug1) : print "Rule Declared: 114"


def p_cond_clause(p):
	'''cond_clause : cond_part statement_s
	'''
        p[0] = {'condition':p[1],'body':p[2]}
	if (Debug1) : print "Rule Declared: 115"


def p_cond_part(p):
	'''cond_part : condition THEN
	'''
        p[0] = p[1]
	if (Debug1) : print "Rule Declared: 116"


def p_condition(p):
	'''condition : expression
	'''
        p[0] = p[1]
	if (Debug1) : print "Rule Declared: 117"


def p_else_opt(p):
	'''else_opt :
	   | ELSE statement_s
	'''
        if len(p) == 1:
            p[0] = None
        else:
            p[0] = p[2]
	if (Debug1) : print "Rule Declared: 118"


def p_case_stmt(p):
	'''case_stmt : case_hdr pragma_s alternative_s END CASE ';'
	'''
	if (Debug1) : print "Rule Declared: 119"


def p_case_hdr(p):
	'''case_hdr : CASE expression IS
	'''
	if (Debug1) : print "Rule Declared: 120"


def p_alternative_s(p):
	'''alternative_s :
	   | alternative_s alternative
	'''
	if (Debug1) : print "Rule Declared: 121"


def p_alternative(p):
	'''alternative : WHEN choice_s ARROW statement_s
	'''
	if (Debug1) : print "Rule Declared: 122"


def p_loop_stmt(p):
	'''loop_stmt : label_opt iteration basic_loop id_opt ';'
	'''
        global curSymTable
        p[0] = deepcopy(p[2])
        p[0]['body'] = p[3]

        if p[0]['stmt'] == 'for' :
            curSymTable = curSymTable.endScope()

	if (Debug1) : print "Rule Declared: 123"


def p_label_opt(p):
	'''label_opt :
	   | IDENTIFIER ':'
	'''
        if len(p) == 1:
            p[0] = None
        else:
            print "Unhandled loop Identifier\n"
            p_error(p)

	if (Debug1) : print "Rule Declared: 124"


def p_iteration(p):
	'''iteration :
	   | WHILE condition
	   | iter_part reverse_opt discrete_range
	'''
        global curSymTable
        if len(p) == 1:
            print "Unhandled Case: Iteration Empty\n"
            p_error(p)
        elif len(p) == 3:
            p[0] = {'stmt': 'while', 'cond':p[2]}
        else:
            ## For
            curSymTable = curSymTable.beginScope('FOR')
            attributes = {'name': p[1], 'type': 'int' }
            curSymTable.newSym(p[1], attributes)

            p[0] = {'stmt': 'for', 'cond': p[3], 'var': p[1] }

	if (Debug1) : print "Rule Declared: 125"


def p_iter_part(p):
	'''iter_part : FOR IDENTIFIER IN
	'''
        p[0] = p[2]
	if (Debug1) : print "Rule Declared: 126"


def p_reverse_opt(p):
	'''reverse_opt :
	   | REVERSE
	'''
	if (Debug1) : print "Rule Declared: 127"


def p_basic_loop(p):
	'''basic_loop : LOOP statement_s END LOOP
	'''
        p[0] = p[2]
	if (Debug1) : print "Rule Declared: 128"


def p_id_opt(p):
	'''id_opt :
	   | designator
	'''
        if len(p)== 1:
            p[0]= None
        else:
            p[0] = p[1]
	if (Debug1) : print "Rule Declared: 129"


def p_block(p):
	'''block : label_opt block_decl block_body END id_opt ';'
	'''
	if (Debug1) : print "Rule Declared: 130"


def p_block_decl(p):
	'''block_decl :
	   | DECLARE decl_part
	'''
	if (Debug1) : print "Rule Declared: 131"


def p_block_body(p):
	'''block_body : BEGIN handled_stmt_s
	'''
        p[0] = p[2]
	if (Debug1) : print "Rule Declared: 132"


def p_handled_stmt_s(p):
	'''handled_stmt_s : statement_s except_handler_part_opt
	'''
        p[0] =p[1]

	if (Debug1) : print "Rule Declared: 133"


def p_except_handler_part_opt(p):
	'''except_handler_part_opt :
	   | except_handler_part
	'''
	if (Debug1) : print "Rule Declared: 134"


def p_exit_stmt(p):
	'''exit_stmt : EXIT name_opt when_opt ';'
	'''
	if (Debug1) : print "Rule Declared: 135"


def p_name_opt(p):
	'''name_opt :
	   | name
	'''
	if (Debug1) : print "Rule Declared: 136"


def p_when_opt(p):
	'''when_opt :
	   | WHEN condition
	'''
	if (Debug1) : print "Rule Declared: 137"


def p_return_stmt(p):
	'''return_stmt : RETURN ';'
	   | RETURN expression ';'
	'''
        if len(p) == 3:
            p[0] = {'isVoid': True}
        else:
            p[0] = {'isVoid':False, 'expres':p[2] }
	if (Debug1) : print "Rule Declared: 138"


def p_goto_stmt(p):
	'''goto_stmt : GOTO name ';'
	'''
	if (Debug1) : print "Rule Declared: 139"


def p_subprog_decl(p):
	'''subprog_decl : subprog_spec ';'
	   | generic_subp_inst ';'
	   | subprog_spec_is_push ABSTRACT ';'
	'''
	if (Debug1) : print "Rule Declared: 140"

def p_subprog_spec(p):
	'''subprog_spec : PROCEDURE compound_name formal_part_opt
	   | FUNCTION designator formal_part_opt RETURN name
	   | FUNCTION designator
	'''
        global curSymTable
        if len(p) == 4:
            p[0] = {}
            p[0]['subprog'] = 'procedure'
            p[0]['procedure'] = p[2]['name']
            if p[3] != None:
                p[0]['params'] = p[3]

            curSymTable.newSym(p[2]['name'], p[0])
            curSymTable = curSymTable.beginScope(p[2]['name'])

            if p[3] != None:
                for var in p[3]:
                    attributes = deepcopy(var['attributes'])
                    for name in var['vars']:
                        attributes['name']= name
                        curSymTable.newSym(name, attributes)


        elif len(p) == 6:

            p[0] = {}
            p[0]['subprog'] = 'function'
            p[0]['function'] = p[2]['name']
            if p[5]['name'].lower() == 'integer':
                p[0]['returnType'] = 'int'
            elif p[5]['name'].lower() == 'character':
                p[0]['returnType'] = 'char'
            else:
                p[0]['returnType'] = p[5]['name']
            if p[3] != None:
                p[0]['params'] = p[3]

            curSymTable.newSym(p[2]['name'], p[0])
            curSymTable = curSymTable.beginScope(p[2]['name'])

            if p[3] != None:
                for var in p[3]:
                    attributes = deepcopy(var['attributes'])
                    for name in var['vars']:
                        attributes['name']= name
                        curSymTable.newSym(name, attributes)

        else:
            p[0] = {}
            p[0]['subprog'] = 'function'
            p[0]['function'] = p[2]['name']

            curSymTable.newSym(p[2]['name'], p[0])
            curSymTable = curSymTable.beginScope(p[2]['name'])


	if (Debug1) : print "Rule Declared: 141"


def p_designator(p):
	'''designator : compound_name
	   | STRING
	'''
        p[0] = deepcopy(p[1])
	if (Debug1) : print "Rule Declared: 142"


def p_formal_part_opt(p):
	'''formal_part_opt :
	   | formal_part
	'''
        if len(p) == 1:
            p[0] = None
        else:
            p[0] = p[1]
	if (Debug1) : print "Rule Declared: 143"


def p_formal_part(p):
	'''formal_part : '(' param_s ')'
	'''
        p[0] = p[2]

	if (Debug1) : print "Rule Declared: 144"


def p_param_s(p):
	'''param_s : param
	   | param_s ';' param
	'''
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[3]]
	if (Debug1) : print "Rule Declared: 145"


def p_param(p):
	'''param : def_id_s ':' mode mark init_opt
	   | error
	'''

        if p[4]['name'].lower() == 'integer':
            p[0] = {'vars':p[1], 'attributes':{'type': 'int'} }
        elif p[4]['name'].lower() == 'character':
            p[0] = {'vars':p[1], 'attributes':{'type': 'char'} }
        else :
            p[0] = {'vars':p[1], 'attributes':{'type': p[4]['name']} }
        if p[5] != None:

            if 'isliteral' in p[5]:
                p[0]['attributes']['val'] = p[5]['val']
            else:
                p[0]['attributes']['expression']= p[5]

        if p[3] != None:
            p[0]['attributes']['mode'] = p[3]



	if (Debug1) : print "Rule Declared: 146"


def p_mode(p):
	'''mode :
	   | IN
	   | OUT
	   | IN OUT
	   | ACCESS
	'''
        if len(p) == 1:
            p[0] = None
        else:
            p[0] = p[1]
	if (Debug1) : print "Rule Declared: 147"


def p_subprog_spec_is_push(p):
	'''subprog_spec_is_push : subprog_spec IS
	'''
        p[0]= p[1]
	if (Debug1) : print "Rule Declared: 148"


def p_subprog_body(p):
	'''subprog_body : subprog_spec_is_push decl_part block_body END id_opt ';'
	'''
        global curStmtNo
        global curSymTable
        curStmtNo += 1
        if p[1]['subprog'] == 'procedure' and p[1]['procedure'] != p[5]['name']:
            print "Procedure Name does not match with END name\n"
            p_error(p)
        elif p[1]['subprog'] == 'function' and p[1]['function'] != p[5]['name']:
            print "Function Name does not match with END name\n"
            p_error(p)

        if p[1]['subprog'] == 'procedure':

            p[0] = {'stmt':'procedure', 'procedure': p[5]['name'], 'decl':p[2], 'body':p[3], 'line': curStmtNo}
            if 'params' in p[1]:
                p[0]['params'] = p[1]['params']

        else:

            ## TODO : Check for Return Error: Naive
            hasReturn = False;
            for stmt in p[3]:
                if stmt['stmt'] == 'return':
                    hasReturn = True;
            if hasReturn and 'returnType' not in p[1]:
                assert(1==2), 'Return type not given for function\n'
            elif 'returnType' in p[1] and not hasReturn:
                assert(1==2), 'No Matching return Statement found\n'

            p[0] = {'stmt':'function', 'function': p[5]['name'], 'decl':p[2], 'body':p[3], 'line': curStmtNo}
            if 'returnType' in p[1]:
                p[0]['returnType'] = p[1]['returnType']
            if 'params' in p[1]:
                p[0]['params'] = p[1]['params']
            # curSymTable = curSymTable.endScope()



	if (Debug1) : print "Rule Declared: 149"


def p_procedure_call(p):
	'''procedure_call : name ';'
	'''
        p[0] = p[1]
	if (Debug1) : print "Rule Declared: 150"


def p_pkg_decl(p):
	'''pkg_decl : pkg_spec ';'
	   | generic_pkg_inst ';'
	'''
	if (Debug1) : print "Rule Declared: 151"


def p_pkg_spec(p):
	'''pkg_spec : PACKAGE compound_name IS decl_item_s private_part END c_id_opt
	'''
	if (Debug1) : print "Rule Declared: 152"


def p_private_part(p):
	'''private_part :
	   | PRIVATE decl_item_s
	'''
	if (Debug1) : print "Rule Declared: 153"


def p_c_id_opt(p):
	'''c_id_opt :
	   | compound_name
	'''
	if (Debug1) : print "Rule Declared: 154"


def p_pkg_body(p):
	'''pkg_body : PACKAGE BODY compound_name IS decl_part body_opt END c_id_opt ';'
	'''
	if (Debug1) : print "Rule Declared: 155"


def p_body_opt(p):
	'''body_opt :
	   | block_body
	'''
	if (Debug1) : print "Rule Declared: 156"


def p_private_type(p):
	'''private_type : tagged_opt limited_opt PRIVATE
	'''
	if (Debug1) : print "Rule Declared: 157"


def p_limited_opt(p):
	'''limited_opt :
	   | LIMITED
	'''
	if (Debug1) : print "Rule Declared: 158"


def p_use_clause(p):
	'''use_clause : USE name_s ';'
	   | USE TYPE name_s ';'
	'''
	if (Debug1) : print "Rule Declared: 159"


def p_name_s(p):
	'''name_s : name
	   | name_s ',' name
	'''
	if (Debug1) : print "Rule Declared: 160"


def p_rename_decl(p):
	'''rename_decl : def_id_s ':' object_qualifier_opt subtype_ind renames ';'
	   | def_id_s ':' EXCEPTION renames ';'
	   | rename_unit
	'''
	if (Debug1) : print "Rule Declared: 161"


def p_rename_unit(p):
	'''rename_unit : PACKAGE compound_name renames ';'
	   | subprog_spec renames ';'
	   | generic_formal_part PACKAGE compound_name renames ';'
	   | generic_formal_part subprog_spec renames ';'
	'''
	if (Debug1) : print "Rule Declared: 162"


def p_renames(p):
	'''renames : RENAMES name
	'''
	if (Debug1) : print "Rule Declared: 163"


def p_task_decl(p):
	'''task_decl : task_spec ';'
	'''
	if (Debug1) : print "Rule Declared: 164"


def p_task_spec(p):
	'''task_spec : TASK simple_name task_def
	   | TASK TYPE simple_name discrim_part_opt task_def
	'''
	if (Debug1) : print "Rule Declared: 165"


def p_task_def(p):
	'''task_def :
	   | IS entry_decl_s rep_spec_s task_private_opt END id_opt
	'''
	if (Debug1) : print "Rule Declared: 166"


def p_task_private_opt(p):
	'''task_private_opt :
	   | PRIVATE entry_decl_s rep_spec_s
	'''
	if (Debug1) : print "Rule Declared: 167"


def p_task_body(p):
	'''task_body : TASK BODY simple_name IS decl_part block_body END id_opt ';'
	'''
	if (Debug1) : print "Rule Declared: 168"


def p_prot_decl(p):
	'''prot_decl : prot_spec ';'
	'''
	if (Debug1) : print "Rule Declared: 169"


def p_prot_spec(p):
	'''prot_spec : PROTECTED IDENTIFIER prot_def
	   | PROTECTED TYPE simple_name discrim_part_opt prot_def
	'''
	if (Debug1) : print "Rule Declared: 170"


def p_prot_def(p):
	'''prot_def : IS prot_op_decl_s prot_private_opt END id_opt
	'''
	if (Debug1) : print "Rule Declared: 171"


def p_prot_private_opt(p):
	'''prot_private_opt :
	   | PRIVATE prot_elem_decl_s
	'''
	if (Debug1) : print "Rule Declared: 172"


def p_prot_op_decl_s(p):
	'''prot_op_decl_s :
	   | prot_op_decl_s prot_op_decl
	'''
	if (Debug1) : print "Rule Declared: 173"


def p_prot_op_decl(p):
	'''prot_op_decl : entry_decl
	   | subprog_spec ';'
	   | rep_spec
	   | pragma
	'''
	if (Debug1) : print "Rule Declared: 174"


def p_prot_elem_decl_s(p):
	'''prot_elem_decl_s :
	   | prot_elem_decl_s prot_elem_decl
	'''
	if (Debug1) : print "Rule Declared: 175"


def p_prot_elem_decl(p):
	'''prot_elem_decl : prot_op_decl
    | comp_decl
	'''
	if (Debug1) : print "Rule Declared: 176"


def p_prot_body(p):
	'''prot_body : PROTECTED BODY simple_name IS prot_op_body_s END id_opt ';'
	'''
	if (Debug1) : print "Rule Declared: 177"


def p_prot_op_body_s(p):
	'''prot_op_body_s : pragma_s
	   | prot_op_body_s prot_op_body pragma_s
	'''
	if (Debug1) : print "Rule Declared: 178"


def p_prot_op_body(p):
	'''prot_op_body : entry_body
	   | subprog_body
	   | subprog_spec ';'
	'''
	if (Debug1) : print "Rule Declared: 179"


def p_entry_decl_s(p):
	'''entry_decl_s : pragma_s
	   | entry_decl_s entry_decl pragma_s
	'''
	if (Debug1) : print "Rule Declared: 180"


def p_entry_decl(p):
	'''entry_decl : ENTRY IDENTIFIER formal_part_opt ';'
	   | ENTRY IDENTIFIER '(' discrete_range ')' formal_part_opt ';'
	'''
	if (Debug1) : print "Rule Declared: 181"


def p_entry_body(p):
	'''entry_body : ENTRY IDENTIFIER formal_part_opt WHEN condition entry_body_part
	   | ENTRY IDENTIFIER '(' iter_part discrete_range ')' formal_part_opt WHEN condition entry_body_part
	'''
	if (Debug1) : print "Rule Declared: 182"


def p_entry_body_part(p):
	'''entry_body_part : ';'
	   | IS decl_part block_body END id_opt ';'
	'''
	if (Debug1) : print "Rule Declared: 183"


def p_rep_spec_s(p):
	'''rep_spec_s :
	   | rep_spec_s rep_spec pragma_s
	'''
	if (Debug1) : print "Rule Declared: 184"


def p_entry_call(p):
	'''entry_call : procedure_call
	'''
	if (Debug1) : print "Rule Declared: 185"


def p_accept_stmt(p):
	'''accept_stmt : accept_hdr ';'
	   | accept_hdr DO handled_stmt_s END id_opt ';'
	'''
	if (Debug1) : print "Rule Declared: 186"


def p_accept_hdr(p):
	'''accept_hdr : ACCEPT entry_name formal_part_opt
	'''
	if (Debug1) : print "Rule Declared: 187"


def p_entry_name(p):
	'''entry_name : simple_name
	   | entry_name '(' expression ')'
	'''
	if (Debug1) : print "Rule Declared: 188"


def p_delay_stmt(p):
	'''delay_stmt : DELAY expression ';'
	   | DELAY UNTIL expression ';'
	'''
	if (Debug1) : print "Rule Declared: 189"


def p_select_stmt(p):
	'''select_stmt : select_wait
	   | async_select
	   | timed_entry_call
	   | cond_entry_call
	'''
	if (Debug1) : print "Rule Declared: 190"


def p_select_wait(p):
	'''select_wait : SELECT guarded_select_alt or_select else_opt  END SELECT ';'
	'''
	if (Debug1) : print "Rule Declared: 191"


def p_guarded_select_alt(p):
	'''guarded_select_alt : select_alt
	   | WHEN condition ARROW select_alt
	'''
	if (Debug1) : print "Rule Declared: 192"


def p_or_select(p):
	'''or_select :
	   | or_select OR guarded_select_alt
	'''
	if (Debug1) : print "Rule Declared: 193"


def p_select_alt(p):
	'''select_alt : accept_stmt stmts_opt
	   | delay_stmt stmts_opt
	   | TERMINATE ';'
	'''
	if (Debug1) : print "Rule Declared: 194"


def p_delay_or_entry_alt(p):
	'''delay_or_entry_alt : delay_stmt stmts_opt
	   | entry_call stmts_opt
	'''
	if (Debug1) : print "Rule Declared: 195"


def p_async_select(p):
	'''async_select : SELECT delay_or_entry_alt THEN ABORT statement_s END SELECT ';'
	'''
	if (Debug1) : print "Rule Declared: 196"


def p_timed_entry_call(p):
	'''timed_entry_call : SELECT entry_call stmts_opt OR delay_stmt stmts_opt END SELECT ';'
	'''
	if (Debug1) : print "Rule Declared: 197"


def p_cond_entry_call(p):
	'''cond_entry_call : SELECT entry_call stmts_opt ELSE statement_s END SELECT ';'
	'''
	if (Debug1) : print "Rule Declared: 198"


def p_stmts_opt(p):
	'''stmts_opt :
	   | statement_s
	'''
	if (Debug1) : print "Rule Declared: 199"


def p_abort_stmt(p):
	'''abort_stmt : ABORT name_s ';'
	'''
	if (Debug1) : print "Rule Declared: 200"


def p_compilation(p):
	'''compilation :
	   | compilation comp_unit
	   | pragma pragma_s
	'''
        if len(p) == 1:
            p[0] = []
        else:
            p[0] = p[1]+ [p[2]]
	if (Debug1) : print "Rule Declared: 201"


def p_comp_unit(p):
	'''comp_unit : context_spec private_opt unit pragma_s
	   | private_opt unit pragma_s
	'''
        if len(p) == 5:
            p[0] = p[3]
        else:
            p[0] = p[2]
	if (Debug1) : print "Rule Declared: 202"


def p_private_opt(p):
	'''private_opt :
	   | PRIVATE
	'''
	if (Debug1) : print "Rule Declared: 203"


def p_context_spec(p):
	'''context_spec : with_clause use_clause_opt
	   | context_spec with_clause use_clause_opt
	   | context_spec pragma
	'''
	if (Debug1) : print "Rule Declared: 204"


def p_with_clause(p):
	'''with_clause : WITH c_name_list ';'
	'''
	if (Debug1) : print "Rule Declared: 205"


def p_use_clause_opt(p):
	'''use_clause_opt :
	   | use_clause_opt use_clause
	'''
	if (Debug1) : print "Rule Declared: 206"


def p_unit(p):
	'''unit : pkg_decl
	   | pkg_body
	   | subprog_decl
	   | subprog_body
	   | subunit
	   | generic_decl
	   | rename_unit
	'''
        p[0] = p[1]
        ######################
        ### AST Generation ###
        ######################

        astFile.write("\t" + 'procedure' + '[ label =' + '\"' + p[0]['procedure']  + '\"''];\n')

        astFile.write("\t" + 'decl' + '[ label =' + '\"' + 'decl'  + '\"''];\n')
        astFile.write("\t" + 'procedure'  + "->" + 'decl' + ";\n")

        astFile.write("\t" + 'body' + '[ label =' + '\"' + 'body'  + '\"''];\n')
        astFile.write("\t" + 'procedure'  + "->" + 'body' + ";\n")

        ### Declarations
        if p[0]['decl'] != None:
            for decl_stmt in p[0]['decl']:
                drawDeclStmt(decl_stmt, 'decl')

        ### Body
        for stat in p[0]['body']:
            drawStmt(stat,'body')

        astFile.write("}\n")

	if (Debug1) : print "Rule Declared: 207"


def p_subunit(p):
	'''subunit : SEPARATE '(' compound_name ')' subunit_body
	'''
	if (Debug1) : print "Rule Declared: 208"


def p_subunit_body(p):
	'''subunit_body : subprog_body
	   | pkg_body
	   | task_body
	   | prot_body
	'''
	if (Debug1) : print "Rule Declared: 209"


def p_body_stub(p):
	'''body_stub : TASK BODY simple_name IS SEPARATE ';'
	   | PACKAGE BODY compound_name IS SEPARATE ';'
	   | subprog_spec IS SEPARATE ';'
	   | PROTECTED BODY simple_name IS SEPARATE ';'
	'''
	if (Debug1) : print "Rule Declared: 210"


def p_exception_decl(p):
	'''exception_decl : def_id_s ':' EXCEPTION ';'
	'''
	if (Debug1) : print "Rule Declared: 211"


def p_except_handler_part(p):
	'''except_handler_part : EXCEPTION exception_handler
	   | except_handler_part exception_handler
	'''
	if (Debug1) : print "Rule Declared: 212"


def p_exception_handler(p):
	'''exception_handler : WHEN except_choice_s ARROW statement_s
	   | WHEN IDENTIFIER ':' except_choice_s ARROW statement_s
	'''
	if (Debug1) : print "Rule Declared: 213"


def p_except_choice_s(p):
	'''except_choice_s : except_choice
	   | except_choice_s '|' except_choice
	'''
	if (Debug1) : print "Rule Declared: 214"


def p_except_choice(p):
	'''except_choice : name
	   | OTHERS
	'''
	if (Debug1) : print "Rule Declared: 215"


def p_raise_stmt(p):
	'''raise_stmt : RAISE name_opt ';'
	'''
	if (Debug1) : print "Rule Declared: 216"


def p_requeue_stmt(p):
	'''requeue_stmt : REQUEUE name ';'
	   | REQUEUE name WITH ABORT ';'
	'''
	if (Debug1) : print "Rule Declared: 217"


def p_generic_decl(p):
	'''generic_decl : generic_formal_part subprog_spec ';'
	   | generic_formal_part pkg_spec ';'
	'''
	if (Debug1) : print "Rule Declared: 218"


def p_generic_formal_part(p):
	'''generic_formal_part : GENERIC
	   | generic_formal_part generic_formal
	'''
	if (Debug1) : print "Rule Declared: 219"


def p_generic_formal(p):
	'''generic_formal : param ';'
	   | TYPE simple_name generic_discrim_part_opt IS generic_type_def ';'
	   | WITH PROCEDURE simple_name formal_part_opt subp_default ';'
	   | WITH FUNCTION designator formal_part_opt RETURN name subp_default ';'
	   | WITH PACKAGE simple_name IS NEW name '(' LESSMORE ')' ';'
	   | WITH PACKAGE simple_name IS NEW name ';'
	   | use_clause
	'''
	if (Debug1) : print "Rule Declared: 220"


def p_generic_discrim_part_opt(p):
	'''generic_discrim_part_opt :
	   | discrim_part
	   | '(' LESSMORE ')'
	'''
	if (Debug1) : print "Rule Declared: 221"


def p_subp_default(p):
	'''subp_default :
	   | IS name
	   | IS LESSMORE
	'''
	if (Debug1) : print "Rule Declared: 222"


def p_generic_type_def(p):
	'''generic_type_def : '(' LESSMORE ')'
	   | RANGE LESSMORE
	   | MOD LESSMORE
	   | DELTA LESSMORE
	   | DELTA LESSMORE DIGITS LESSMORE
	   | DIGITS LESSMORE
	   | array_type
	   | access_type
	   | private_type
	   | generic_derived_type
	'''
	if (Debug1) : print "Rule Declared: 223"


def p_generic_derived_type(p):
	'''generic_derived_type : NEW subtype_ind
	   | NEW subtype_ind WITH PRIVATE
	   | ABSTRACT NEW subtype_ind WITH PRIVATE
	'''
	if (Debug1) : print "Rule Declared: 224"


def p_generic_subp_inst(p):
	'''generic_subp_inst : subprog_spec IS generic_inst
	'''
	if (Debug1) : print "Rule Declared: 225"


def p_generic_pkg_inst(p):
	'''generic_pkg_inst : PACKAGE compound_name IS generic_inst
	'''
	if (Debug1) : print "Rule Declared: 226"


def p_generic_inst(p):
	'''generic_inst : NEW name
	'''
	if (Debug1) : print "Rule Declared: 227"


def p_rep_spec(p):
	'''rep_spec : attrib_def
	   | record_type_spec
	   | address_spec
	'''
	if (Debug1) : print "Rule Declared: 228"


def p_attrib_def(p):
	'''attrib_def : FOR mark USE expression ';'
	'''
	if (Debug1) : print "Rule Declared: 229"


def p_record_type_spec(p):
	'''record_type_spec : FOR mark USE RECORD align_opt comp_loc_s END RECORD ';'
	'''
	if (Debug1) : print "Rule Declared: 230"


def p_align_opt(p):
	'''align_opt :
	   | AT MOD expression ';'
	'''
	if (Debug1) : print "Rule Declared: 231"


def p_comp_loc_s(p):
	'''comp_loc_s :
	   | comp_loc_s mark AT expression RANGE range ';'
	'''
	if (Debug1) : print "Rule Declared: 232"


def p_address_spec(p):
	'''address_spec : FOR mark USE AT expression ';'
	'''
	if (Debug1) : print "Rule Declared: 233"


def p_code_stmt(p):
	'''code_stmt : qualified ';'
	'''
	if (Debug1) : print "Rule Declared: 234"

def p_error(p):
    global parser
    print "line :",p.lineno,"-Parsing Error Found at Token:",p.type
    parser.errok()
