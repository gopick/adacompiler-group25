#!/usr/bin/env python
import sys
import lex
import yacc
import re
import sys
from  adaTokens import *
from grammer import *
from code import tac_to_code

parser = yacc.yacc(start = 'start_symbol', debug = False)
dotInfo  = sys.argv[1][:-4] + '.reduce'

lexer = lex.lex()

inputFile = open(sys.argv[1],'r')
data = inputFile.read()
parser = yacc.yacc(start = 'start_symbol', debug= False)
lexer.input(data)
result = parser.parse(data, dotInfo, debug = 1)

[data, bss, proc, code] = tac_to_code(tac)

asmFileName = sys.argv[1].split('/')[2].split('.')[0]
asmFile = open( '../tests/' + asmFileName + '.asm', 'w')

for item in data:
    asmFile.write(item)
    asmFile.write('\n')

asmFile.write('\n')

for item in bss:
    asmFile.write(item)
    asmFile.write('\n')

asmFile.write('\n')

for item in proc:
    asmFile.write(item)
    asmFile.write('\n')


for item in code:
    asmFile.write(item)
    asmFile.write('\n')

asmFile.close()
