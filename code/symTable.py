from copy import deepcopy
def pretty(d, filePoint, indent = 0):
       for key, value in d.iteritems():
           # print '\t' * indent + str(key)
           printto =  '\t' * indent + str(key)+'\n'
           filePoint.write(printto)
           if isinstance(value, dict):
               pretty(value,filePoint, indent+1)
           else:
               # print '\t' * (indent+1) + str(value)
               printto =  '\t' * (indent+1) + str(value)+'\n'
               filePoint.write(printto)

class symTable:
    def __init__(self, parent = None, name= 'MAIN'):
        self.symbols = {}
        self.parent = parent
        self.childList = []
        self.name = name

    def newSym(self, name, attributes):

        name = name.lower()
        symbols = self.symbols

        if name in symbols:
            print "Symbol: ",name," : already in Symbol Table\n"
            assert(1==2)
        else:
            symbols[name] = {}
            for item in attributes:
                symbols[name][item] = attributes[item]

        self.symbols = symbols

    def updateSym(self, name, attribute, val):

        name = name.lower()

        curTable = self

        notFound = True
        while curTable !=None:
            if name in curTable.symbols:
                curTable.symbols[name][attribute] = val
                notFound= False
                break
            else:
                curTable = curTable.parent

        if notFound:
            print "Symbol: ", name," : used before declaration\n"

    def addtoParentchildlist(self, table):
        self.parent.addtochildList(table)

    def addtochildList(self, item):
        self.childList.append(item)

    def getProcTable(self, proc):
        for table in self.childList:
            if table.name == proc:
                return table
        assert(1==2), "proc not found in symbol table\n"


    def getSym(self, name):

        name = name.lower()

        curTable = self

        while curTable !=None:
            if name in curTable.symbols:
                return curTable.symbols[name]
            else:
                curTable = curTable.parent

        # print "Symbol: ", name," : not found\n"
        return None

    def printTable(self):
        ## To Print Parent Also

        for item in self.symbols:
            print item, ': \n'
            for attribute in self.symbols[item]:
                print "\t", attribute,": ",self.symbols[item][attribute],"  "
            print '\n'

    def printCSV(self, filePoint):
        ## To Print Parent Also

        for item in self.symbols:
            filePoint.write(item)
            filePoint.write('\n')
            print item, ': \n'
            for attribute in self.symbols[item]:
                print "\t", attribute,": ",self.symbols[item][attribute],"  "
                filePoint.write(attribute)
                filePoint.write(',')
                # filePoint.write(self.symbols[item][attribute])
                value = self.symbols[item][attribute]
                if isinstance(value, dict):
                    pretty(value, filePoint)
                else:
                    # print '\n\n',type(value),'\n\n'
                    filePoint.write(str(value))
                filePoint.write('\n')
            print '\n'
            filePoint.write('\n')

class curTable:
    def __init__(self):
        self.symbolTable = symTable()

    def newSym(self, name, attributes):
        self.symbolTable.newSym(name,attributes)

    def updateSym(self, name, attribute, val):
        self.symbolTable.updateSym(name, attribute, val)

    def getSym(self,name):
        return self.symbolTable.getSym(name)

    def printTable(self):
        self.symbolTable.printTable()

    def printCSV(self, filePoint):
        self.symbolTable.printCSV(filePoint)

    def getName(self):
        return self.symbolTable.name;

    def getProcTable(self, proc):
        return self.symbolTable.getProcTable(proc)

    def beginScope(self, name):
        newTable = symTable(self.symbolTable,name)
        self.symbolTable = newTable
        return self

    def endScope(self):
        self.symbolTable.addtoParentchildlist(self.symbolTable)
        # self.symbolTable.parent.childList.append(self)
        self.symbolTable = self.symbolTable.parent
        return self
