
class tac:
    def __init__(self):
        self.nextInstr = 0
        self.tacList = []

    def emit(self, result, operand1, operator, operand2):
        self.tacList.append([str(self.nextInstr),str(result), str(operand1), str(operator), str(operand2)])
        self.nextInstr += 1

    def getNextInstr(self):
        return self.nextInstr

    def getInstrList(self):
        return self.tacList

    def printtacList(self):
        print "\n\nTHREE ADDRESS CODE\n\n"
        for item in self.tacList:
            print item
        print "\n\n"

    def toFile(self):
        f = open('../tests/test.tac','w')
        f.write("THREE ADDRESS CODE\n\n")
        for item in self.tacList:
            for i in item:
                f.write(i)
                f.write(', \t')
            f.write('\n')

        f.close()
