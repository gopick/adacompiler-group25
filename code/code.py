from grammer import *
from copy import deepcopy

code = []
bss= []
data= []
proc = []
codeTemp = []
mainName = "main"
returnVars = []
inMain = True
localVars = []
retAdr = "retAdr"

progVars = []
nameCount = 0
labelCount = 0
procList = []

byte = {'int':4,'float':8}

def reqBytes(type):
    byte = {'int': 'resb 4', 'float': 'resb 8', 'char':'resb 2', 'bool':'resb 4', 'acint': 'resb 4'}
    return byte[type]


def movetoReg( reg, val):

    operandRaw= val.split('(')
    # print "haha\n", operandRaw, reg
    operand = operandRaw[0]
    inSym = curSymTable.getSym(operand)

    if len(operandRaw) > 1:
        refOperand = operandRaw[1].split(')')[0]
        code.append("\tpush esi")
        code.append("\tpush edi")
        code.append("\tmov esi,"+operand)
        inSym2 = curSymTable.getSym(refOperand)
        if inSym2 is None:
            code.append("\tmov edi,"+refOperand)
        else:
            code.append("\tmov edi,["+refOperand+"]")

        # code.append("\tmov ecx,"+str(byte[inSym['type']]))
        # code.append("\tmul ecx")
        code.append("\tmov "+reg+",[esi+edi]")
        code.append("\tpop edi")
        code.append("\tpop esi")

    elif inSym is None:
        code.append("\tmov "+reg+","+ operand)
    else:
        code.append("\tmov "+ reg + ",["+ operand+ "]")

def addtobss( result):
    progVars.append(result)
    inSym = curSymTable.getSym(result)
    bss.append("\t" + result +" " + reqBytes(inSym['type']) )

def newName():
    global nameCount
    nameCount += 1
    return "_varName"+str(nameCount)

def newLabel():
    global labelCount
    labelCount += 1
    return ".labelName"+str(labelCount)

def tac_to_code(tac):

    global curSymTable
    global code
    global bss
    global data
    global procs
    global returnVars
    global inMain
    global localVars
    global retAdr


    data.append("section .data")
    data.append("\tnewline db 0xa")
    data.append("\tnewlinelen equ $-newline")

    data.append("\tminus db \"-\", 0xA, 0xD")

    bss.append("section .bss")

    proc.append("section .text")
    proc.append("\tglobal main\n")
    code.append("main:")
    mainFlag = True;

    for instr in tac.getInstrList():

        resultRaw= instr[1].split('(')
        if len(resultRaw) > 1:
            refResult = resultRaw[1].split(')')[0]
        result = resultRaw[0]
        operand1 = instr[2]
        operand2 = instr[4]
        operator = instr[3]

        code.append('I'+ str(instr[0])+ ':')
        resSym = curSymTable.getSym(result)


        if operator ==  ":=":
            if result not in progVars:
                if 'isArray' in resSym:
                    dim = resSym['dimension']
                    size = 1
                    for i in range(dim):
                        tempsize = resSym['size'][i]['endVal']['val']-resSym['size'][i]['startVal']['val'] + 1
                        size = size*tempsize
                    size= size*byte[resSym['type']]
                    bss.append("\t"+result+" resb "+str(size))
                    localVars.append(result)
                    progVars.append(result)
                else:
                    if not inMain:
                        localVars.append(result)
                    addtobss(result)

            if result[0:3] == '_d_':

                orig_res = result[3:]
                if orig_res not in progVars:
                    if not inMain:
                        localVars.append(orig_res)
                    addtobss(orig_res)

                movetoReg('eax', operand1)
                movetoReg('ebx', orig_res)
                code.append('\tmov [ebx],eax')

            elif operand1 != 'None':
                if 'isArray' in resSym:
                    inSym = curSymTable.getSym(refResult)
                    code.append("\tmov esi,"+result)
                    if inSym is None:
                        code.append("\tmov eax,"+refResult)
                    else:
                        code.append("\tmov eax,["+refResult+"]")

                    # code.append("\tmov ecx,"+str(byte[resSym['type']]))
                    # code.append("\tmul ecx")

                    # inSym = curSymTable.getSym(operand1)
                    # if inSym is None:
                        # code.append("\tmov ebx,"+operand1)
                    # else:
                        # code.append("\tmov ebx,["+operand1+"]")

                    movetoReg('ebx',operand1)

                    code.append("\tmov [esi+eax],ebx")


                elif resSym['type'] == 'int' or resSym['type'] == 'acint':
                    movetoReg('eax', operand1)
                    code.append("\tmov ["+ result + "],eax")

                elif resSym['type'] == 'float':
                    inSym = curSymTable.getSym(operand1)
                    if inSym is None:
                        temp = newName()
                        data.append("\t"+temp+" dq "+operand1)
                        code.append("\tfld qword["+temp+"]")
                    else:
                        code.append("\tfld qword["+operand1+"]")

                    code.append("\tfstp qword["+result+"]")
                else:
                    assert(1==2), "Assignment not handled for non int/float/pointer types\n"


        elif operator in [ '+int', '-int' ]:

            if result not in progVars:
                if not inMain:
                    localVars.append(result)
                addtobss(result)
            movetoReg('eax', operand1)
            movetoReg('edx', operand2)
            if operator  == '+int':
                code.append("\tadd eax,edx")
            elif operator == '-int':
                code.append("\tsub eax,edx")
            code.append("\tmov [" + result + "],eax")

        elif operator == 'access':
            if result not in progVars:
                if not inMain:
                    localVars.append(result)
                addtobss(result)

            code.append("\tmov eax,"+operand1)
            code.append("\tmov ["+result+"],eax")

        elif operator in [ '+float', '-float' ]:

            if result not in progVars:
                if not inMain:
                    localVars.append(result)
                addtobss(result)

            inSym = curSymTable.getSym(operand1)
            if inSym is None:
                temp = newName()
                data.append("\t"+temp+" dq "+operand1)
                code.append("\tfld qword["+temp+"]")
            else:
                code.append("\tfld qword["+operand1+"]")


            inSym = curSymTable.getSym(operand2)
            if inSym is None:
                temp = newName()
                data.append("\t"+temp+" dq "+operand2)
            else:
                temp = operand2

            if operator  == '+float':
                code.append("\tfadd qword["+temp+"]")
            elif operator == '-float':
                code.append("\tfsub qword["+temp+"]")

            code.append("\tfstp qword["+result+"]")

        elif operator in [ '*', '/' ]:

            if result not in progVars:
                if not inMain:
                    localVars.append(result)
                addtobss(result)

            resSym = curSymTable.getSym(result)
            if resSym['type'] == 'float':
                inSym = curSymTable.getSym(operand1)
                if inSym is None:
                    temp = newName()
                    data.append("\t"+temp+" dq "+operand1)
                    code.append("\tfld qword["+temp+"]")
                else:
                    code.append("\tfld qword["+operand1+"]")


                inSym = curSymTable.getSym(operand2)
                if inSym is None:
                    temp = newName()
                    data.append("\t"+temp+" dq "+operand2)
                else:
                    temp = operand2

                if operator  == '*':
                    code.append("\tfmul qword["+temp+"]")
                elif operator == '/':
                    code.append("\tfdiv qword["+temp+"]")

                code.append("\tfstp qword["+result+"]")
            elif resSym['type'] == 'int':

                if operator == '*':

                    movetoReg('eax', operand1)
                    movetoReg('edx', operand2)
                    code.append("\timul edx")
                    code.append("\tmov [" + result + "],eax")

                elif operator == '/':
                    # code.append("\tmov edx,0")
                    movetoReg('eax',operand1)
                    code.append("\tcdq")
                    # code.append("push eax")
                    # code.append("rol eax")
                    # code.append("mov edx,eax")
                    # code.append("pop eax")
                    movetoReg('ebx', operand2)
                    code.append("\tidiv ebx")
                    code.append("\tmov [" + result + "],eax")

        elif operator == 'goto':

            if result == 'None':
                pass
            elif result[0:13] == 'Begin_Proced_':
                if mainFlag:
                    mainFlag = False
                    mainName = result[13:]
                else:
                    localVars = []
                    codeTemp = deepcopy(code)
                    code = []
                    procName = result[13:]
                    code.append(procName+":")
                    code.append("\tpop edi")
                    retAdr = procName + "retAdr"
                    bss.append("\t"+retAdr+" resb  4")
                    code.append("\tmov ["+retAdr+"],edi")
                    procSym = curSymTable.getSym(procName)
                    #TODO change symbol table
                    # print curSymTable.getName()
                    curSymTable.symbolTable = curSymTable.getProcTable(procName)
                    #TODO pop all parameters into memory locations
                    for comp_param in procSym['params']:
                        for var in comp_param['vars']:
                            addtobss(var)
                            if comp_param['attributes']['mode'] == 'in':
                                localVars.append(var)
                                code.append("\tpop eax")
                                code.append("\tmov ["+var+"],eax")
                            else:
                                returnVars.append(var)

                    inMain = False

            elif result[0] == '_':
                movetoReg('eax', result)
                code.append("\tcmp eax,0")
                target = 'I' + operand1
                code.append("\tjg "+target)
            else:
                target = 'I' + result
                code.append("\tjmp "+target)

        elif result[0:12] == 'endprocedure':

            if result[12:] != mainName:
                procName = result[12:]
                for item in code:
                    proc.append(item)
                #TODO push all return values
                for outvar in returnVars:
                    proc.append("\tmov eax,["+outvar+"]")
                    proc.append("\tpush eax")

                returnVars = []

                #TODO change the symbol table back
                # print curSymTable.symbolTable
                curSymTable.symbolTable = curSymTable.symbolTable.parent

                proc.append("\tmov edi,["+retAdr+"]")
                proc.append("\tpush edi")
                proc.append('\tret')
                code = deepcopy(codeTemp)
                inMain = True


        elif operator in [ '<=','>=', ">", "<" , "=", "/=" ]:

            if result not in progVars:
                if not inMain:
                    localVars.append(result)
                addtobss(result)

            ## Assuming both operands are not constants

            inSym = curSymTable.getSym(operand1)
            inSym2 = curSymTable.getSym(operand2)
            if inSym is not None:
                tempType = inSym['type']
            elif inSym2 is not None:
                tempType = inSym2['type']
            else:
                tempType = 'int'

            if tempType == 'float':

                if inSym == None:
                    tempName = newName()
                    data.append("\t"+tempName+" dq "+str(operand1))
                    code.append("\tfld qword[" + tempName+"]")
                else:
                    code.append("\tfld qword[" + operand1+"]")

                if inSym2 == None:
                    tempName = newName()
                    data.append("\t"+tempName+" dq "+str(operand2))
                    code.append("\tfld qword[" + tempName+"]")
                else:
                    code.append("\tfld qword[" + operand2+"]" )

                code.append("\tfcomip")
                code.append("\tffree st0")

                target = newLabel()
                target2 = newLabel()

                if operator == '<=':
                    code.append("\tjae "+target)
                elif operator == '>=':
                    code.append("\tjbe "+target)
                elif operator == '>':
                    code.append("\tjb "+target)
                elif operator == '<':
                    code.append("\tja "+target)
                elif operator == '=':
                    code.append("\tje "+target)
                elif operator == '\=':
                    code.append("\tjne "+target)

                code.append("\tmov eax,0")
                code.append("\tmov ["+result+"],eax")
                code.append("\tjmp "+target2)
                code.append(target+":")
                code.append("\tmov eax,1")
                code.append("\tmov ["+result+"],eax")
                code.append(target2+":")

            else :

                movetoReg('eax', operand1)
                movetoReg('edx', operand2)
                code.append("\tcmp eax,edx")
                target = newLabel()
                target2 = newLabel()

                if operator == '<=':
                    code.append("\tjle "+target)
                elif operator == '>=':
                    code.append("\tjge "+target)
                elif operator == '>':
                    code.append("\tjg "+target)
                elif operator == '<':
                    code.append("\tjl "+target)
                elif operator == '=':
                    code.append("\tje "+target)
                elif operator == '\=':
                    code.append("\tjne "+target)

                code.append("\tmov eax,0")
                code.append("\tmov ["+result+"],eax")
                code.append("\tjmp "+target2)
                code.append(target+":")
                code.append("\tmov eax,1")
                code.append("\tmov ["+result+"],eax")
                code.append(target2+":")


        elif operator == 'proced_call':

            if operand1 == 'Put_Line':
                operand2 =  operand2.split('\'')[1]
                temp = newName()
                data.append("\t"+temp+" db "+operand2)
                data.append("\tlen"+temp+" equ $-"+temp)

                code.append("\tmov eax,4")
                code.append("\tmov ebx,1")
                code.append("\tmov ecx,"+temp)
                code.append("\tmov edx,len"+temp)
                code.append("\tint 0x80")
            elif operand1 == 'open':
                filename =  operand2.split('\'')[1][1:-1]
                filedes =  operand2.split('\'')[3]

                tempfile = newName()
                data.append("\t"+tempfile+" db \""+filename+"\",0")

                code.append("\tmov eax,8")
                code.append("\tmov ebx,"+tempfile)
                code.append("\tmov ecx,0777")
                code.append("\tint 0x80")

                code.append("\tmov ["+filedes+"],eax")

            elif operand1 == 'write':
                line1 =  operand2.split('\'')[1][1:-1]
                filedes =  operand2.split('\'')[3]

                templine = newName()
                data.append("\t"+templine+" db \""+line1+"\",0xa")
                templen = newName()
                data.append("\t"+templen+" equ $-"+templine)

                code.append("\tmov edx,"+templen)
                code.append("\tmov ecx,"+templine)
                code.append("\tmov ebx,["+filedes+"]")
                code.append("\tmov eax,4")
                code.append("\tint 0x80")

            elif operand1 == 'close':

                filedes =  operand2.split('\'')[1]
                code.append("\tmov eax,6")
                code.append("\tmov ebx,["+filedes+"]")
                code.append("\tint 0x80")


            elif operand1 == 'Put':
                operand2 =  operand2.split('\'')[1]
                inSym = curSymTable.getSym(operand2)

                flagFloat = True
                if '.' in operand2:
                    floagFloat = False

                if flagFloat and (inSym is None or inSym['type'] == 'int'):

                    movetoReg('eax', operand2)
                    code.append('\tcall Put')

                    if 'Put' not in procList:
                        procList.append('Put')

                        proc.append('Put:')
                        proc.append('\tcmp eax,0')
                        proc.append('\tjge .put_L2')
                        proc.append('\tneg eax')
                        proc.append('\tmov ecx,minus')
                        proc.append('\tmov edx, 1')
                        proc.append('\tpush eax')
                        proc.append('\tmov eax, 4')
                        proc.append('\tmov ebx, 1')
                        proc.append('\tint 0x80')
                        proc.append('\tpop eax')
                        proc.append('\t.put_L2:')
                        proc.append('\tsub esp, 16')
                        proc.append('\tmov ecx, 10')
                        proc.append('\tmov ebx, 16')
                        proc.append('\t.put_L1:')
                        proc.append('\txor edx, edx')
                        proc.append('\tdiv ecx')
                        proc.append('\tor dl, 0x30')
                        proc.append('\tsub ebx, 1')
                        proc.append('\tmov [esp+ebx], dl')
                        proc.append('\ttest eax,eax')
                        proc.append('\tjnz .put_L1')
                        proc.append('\tmov eax, 4')
                        proc.append('\tlea ecx, [esp+ebx]')
                        proc.append('\tmov edx, 16')
                        proc.append('\tsub edx, ebx')
                        proc.append('\tmov ebx, 1')
                        proc.append('\tint 0x80')
                        proc.append('\tadd esp, 16')
                        proc.append('\tret')
                else:

                    if 'PutF' not in procList:
                        procList.append("PutF")
                        proc.append("extern printf, scanf")
                        data.append("print_format db \"%f\",0xA,0")

                    tempFloatName = operand2

                    if not flagFloat:
                        tempName = newName()
                        data.append("\t"+tempName+" resq "+operand2)
                        tempFloatName = tempName

                    code.append("\tfld qword ["+tempFloatName+"]")
                    code.append("\tfstp qword [esp]")
                    code.append("\tpush print_format")
                    code.append("\tcall printf")
                    code.append("\tadd esp,12")



            elif operand1 == 'Get':
                operand2 =  operand2.split('\'')[1]
                inSym = curSymTable.getSym(operand2)
                temp = newName()
                bss.append("\t" + temp +" " + reqBytes(inSym['type']) )
                code.append("\tmov eax,3")
                code.append("\tmov ebx,0")
                code.append("\tmov ecx,"+temp)
                code.append("\tmov edx,11")
                code.append("\tint 0x80")
                code.append("\tmov edx,"+temp)

                if 'Get' not in procList:
                    procList.append('Get')
                    proc.append("Get:")
                    proc.append("\txor eax,eax")
                    proc.append("\t.top:")
                    proc.append("\tmovzx ecx,byte[edx]")
                    proc.append("\tinc edx")
                    proc.append("\tcmp ecx,\'0\'")
                    proc.append("\tjb .done")
                    proc.append("\tcmp ecx,\'9\'")
                    proc.append("\tja .done")
                    proc.append("\tsub ecx,\'0\'")
                    proc.append("\timul eax,10")
                    proc.append("\tadd eax,ecx")
                    proc.append("\tjmp .top")
                    proc.append("\t.done:")
                    proc.append("\tret")

                code.append("\tcall Get")
                code.append("\tmov ["+operand2+"],eax")
            elif operand1 == 'New_Line':
                operand2 =  int(operand2.split('\'')[1])
                for i in range(operand2):
                    code.append("\tmov ecx, newline")
                    code.append("\tmov edx, newlinelen")
                    movetoReg('eax', '4')
                    movetoReg('ebx', '1')
                    code.append('\tint 0x80')
            else :
                inList = []
                outList = []
                operand2 = operand2.split('[')[1].split(']')[0].split(',')
                procSym = curSymTable.getSym(operand1)
                for item in range(len(operand2)):
                    if procSym['params'][item]['attributes']['mode'] =='in':
                        inList.append(operand2[item].split('\'')[1].strip())
                    else:
                        outList.append(operand2[item].split('\'')[1].strip())
                inList.reverse()
                outList.reverse()

                if not inMain:
                    code.append("\tmov eax,["+retAdr+"]")
                    code.append("\tpush eax")
                    for var in localVars:
                        varSym = curSymTable.getSym(var)
                        if 'isArray' not in varSym and var not in outList:
                            movetoReg('eax', var)
                            code.append("\tpush eax")
                        else :
                            pass
                            ## TODO Handle array pushing

                    #push local variables

                #push parameters
                # for comp_param in procSym['params']:
                    # for var in comp_param['vars']:
                        # if comp_param['attributes']['mode'] == 'in':
                            # inList.append(var)
                            # pass
                        # else:
                            # outList.append(var)
                for var in inList:
                    # code.append("\tmov eax,["+var+"]")
                    # print var.split('\'')[1]
                    movetoReg('eax', var )
                    code.append("\tpush eax")
                code.append("\tcall "+operand1)
                #pop return values and store into some temps ### Remove pull tacs
                for var in outList:
                    code.append("\tpop eax")
                    code.append("\tmov ["+var+"],eax")
                localVars2 = deepcopy(localVars)
                localVars.reverse()
                if not inMain:
                    for var in localVars:
                        varSym = curSymTable.getSym(var)
                        if 'isArray' not in varSym and var not in outList:
                            code.append("\tpop eax")
                            code.append("\tmov ["+var+"],eax")
                        else :
                            pass
                    code.append("\tpop eax")
                    code.append("\tmov ["+retAdr+"],eax")
                localVars = deepcopy(localVars2)



    code.append("\tmov ecx, newline")
    code.append("\tmov edx, newlinelen")
    movetoReg('eax', '4')
    movetoReg('ebx', '1')
    code.append('\tint 0x80')
    code.append('\tmov eax,1')
    code.append('\tint 0x80')
    return [data, bss, proc, code]


