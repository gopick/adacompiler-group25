#!/usr/bin/env python
import sys
import lex
import yacc
import re
import sys
from  adaTokens import *
from grammer import *
from makeDot import *

parser = yacc.yacc(start = 'start_symbol', debug = True)
dotInfo  = sys.argv[1][:-4] + '.reduce'

lexer = lex.lex()

inputFile = open(sys.argv[1],'r')
data = inputFile.read()
parser = yacc.yacc(start = 'start_symbol', debug= True)
lexer.input(data)
result = parser.parse(data, dotInfo, debug = 1)
make_ParseTree(dotInfo, sys.argv[1][:-4])


